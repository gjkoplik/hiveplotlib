.. hiveplotlib documentation master file, created by
   sphinx-quickstart on Tue Sep 29 10:26:45 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Hiveplotlib Documentation
=========================

.. toctree::
   :caption: Hive Plots
   :maxdepth: 2

   introduction_to_hive_plots
   quick_hive_plots
   basic_usage
   comparing_network_subgroups
   hive_plots_for_large_networks
   hive_plots_more_than_three_groups
   customizing_edge_curves
   hive_plot_viz_outside_matplotlib

.. toctree::
   :caption: Hive Plot Examples
   :maxdepth: 2

   karate_club
   networkx_examples
   bitcoin_user_ratings

.. toctree::
   :caption: Polar Parallel Coordinates Plots
   :maxdepth: 2

   introduction_to_p2cps
   datashading_p2cps
   p2cp_viz_outside_matplotlib

.. toctree::
   :caption: P2CP Examples
   :maxdepth: 2

   election_96
   correlations

.. toctree::
   :caption: Hiveplotlib Documentation
   :maxdepth: 2

   autodoc

.. toctree::
   :caption: Release Notes
   :maxdepth: 2

   changelog