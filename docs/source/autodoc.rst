Hive Plots
----------

Node, Axis, and HivePlot Classes
================================
.. autoclass:: hiveplotlib.Node
    :members:
.. autoclass:: hiveplotlib.Axis
    :members:
.. autoclass:: hiveplotlib.HivePlot
    :members:

Quick Hive Plots
================
.. autofunction:: hiveplotlib.hive_plot_n_axes

Converters
==========
.. automodule:: hiveplotlib.converters
   :members:

Utility Functions
=================

Helper static methods for working with node data.

.. autofunction:: hiveplotlib.node.dataframe_to_node_list

.. autofunction:: hiveplotlib.node.split_nodes_on_variable

.. automodule:: hiveplotlib.utils
   :members:

Polar Parallel Coordinates Plots
--------------------------------

P2CP Class
==========
.. autoclass:: hiveplotlib.P2CP
   :members:

Quick P2CPs
================
.. autofunction:: hiveplotlib.p2cp_n_axes

Utility Functions
=================

Helper static methods for generating and working with ``P2CP`` instances.

.. autofunction:: hiveplotlib.p2cp.indices_for_unique_values

.. autofunction:: hiveplotlib.p2cp.split_df_on_variable

Visualization
-------------

Matplotlib
==========
.. automodule:: hiveplotlib.viz.matplotlib
   :members:

Bokeh
=====
.. automodule:: hiveplotlib.viz.bokeh
   :members:

Plotly
======
.. automodule:: hiveplotlib.viz.plotly
   :members:

Datashader in Matplotlib
========================
.. automodule:: hiveplotlib.viz.datashader
   :members:

Example Datasets
----------------
.. automodule:: hiveplotlib.datasets
   :members: