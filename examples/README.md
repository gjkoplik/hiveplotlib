# Examples

Example `jupyter` notebooks calling `hiveplotlib` code

If trying to run the example notebooks, note these are maintained to run in the conda
environment in this repository, specified by `hiveplot_env.yml`.

To install this `conda` environment and associated `jupyter` kernel, run:

```
$ cd <path/to/repository>
$ bash install.sh
```

(Note, this assumes an installed `anaconda` or `miniconda` distribution.)