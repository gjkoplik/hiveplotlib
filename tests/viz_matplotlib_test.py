# viz_matplotlib_test.py

"""
Tests for ``hiveplotlib.viz.__init__.py``, specifically the ``matplotlib``-backend visualization functions.

Note, we would probably eventually want something that actually compares images
(e.g. ``pytest-mpl``), but image comparison tests, particularly when building base images
locally and comparing to images generated in the CI for `matplotlib`, almost inevitably
leads to endless failures that shouldn't be failures. For now, we will simply test that
the viz functions run on reasonably complex examples.
"""

import traceback
import warnings
from typing import Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pytest
from hiveplotlib import P2CP, Axis, HivePlot, Node, p2cp_n_axes
from hiveplotlib.viz import (
    axes_viz,
    edge_viz,
    hive_plot_viz,
    label_axes,
    node_viz,
    p2cp_legend,
    p2cp_viz,
)

pytestmark = pytest.mark.unmarked


def three_axis_hiveplot_example() -> Tuple[HivePlot, np.ndarray]:
    """
    3 axis example ``HivePlot`` for use in multiple tests later and simulated edges.

    :return: ``hiveplotlib.HivePlot`` instance, ``(n, 2)`` array  of edges (ids).
    """
    hp = HivePlot()

    # build nodes
    node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
    node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
    node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
    node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
    node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
    node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

    nodes = [node0, node1, node2, node3, node4, node5]

    hp.add_nodes(nodes)

    # build axes
    axis0 = Axis(axis_id="A", start=1, end=5, angle=0)
    axis1 = Axis(axis_id="B", start=1, end=5, angle=120)
    axis2 = Axis(axis_id="C", start=1, end=5, angle=240)

    axes = [axis0, axis1, axis2]

    hp.add_axes(axes)

    # dummy of from, to pairs
    rng = np.random.default_rng(0)
    edges = rng.choice(np.arange(5).astype(str), size=100).reshape(-1, 2)

    return hp, edges


def three_axis_p2cp_example() -> P2CP:
    """
    3 axis example ``P2CP`` for use in multiple tests later.

    :return: ``hiveplotlib.P2CP`` instance.
    """
    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    return p2cp_n_axes(data=data)


@pytest.mark.parametrize(
    "viz_function", [axes_viz, label_axes, node_viz, edge_viz, hive_plot_viz]
)
def test_viz_functions_run_hiveplot(viz_function: callable) -> None:
    """
    Make sure the baseline viz functions can run without error when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    hp, edges = three_axis_hiveplot_example()

    # hardcoded partition of nodes assignment, placed on axes
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        unique_ids=["0", "1"],
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        unique_ids=["2", "3"],
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        unique_ids=["4", "5"],
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # connect some edges
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C")
    # throw in an edge kwarg and leave axes on for coverage
    hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B", color="C0")

    try:
        viz_function(hp, axes_off=False)
        plt.close()
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail(f"`{viz_function}` should have run, but failed for some reason.")


def test_fill_in_empty_edge_kwargs() -> None:
    """
    Make sure entirely unspecified edge kwargs doesn't break ``hiveplotlib.viz.edge_viz()``.
    """
    hp, edges = three_axis_hiveplot_example()

    # hardcoded partition of nodes assignment, placed on axes
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        unique_ids=["0", "1"],
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        unique_ids=["2", "3"],
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        unique_ids=["4", "5"],
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # connect some edges without ever touching kwargs
    #  (`HivePlot.connect_axes()` will add empty kwargs if none included)
    tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.add_edge_curves_between_axes(axis_id_1="A", axis_id_2="B", tag=tag)

    try:
        edge_viz(instance=hp)
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail("`edge_viz` should have run, but failed for some reason.")


@pytest.mark.parametrize("viz_function", [axes_viz, label_axes])
def test_warnings_no_axes_hive_plot(viz_function: callable) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    hp = HivePlot()

    with pytest.warns() as record:
        viz_function(hp)
        plt.close()

    # confirm we get a single warning
    assert (
        len(record) == 1
    ), f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"


@pytest.mark.parametrize("viz_function", [node_viz, edge_viz, hive_plot_viz])
def test_warnings_missing_content_hive_plot(viz_function: callable) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for hive plots.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    hp, edges = three_axis_hiveplot_example()

    # only place nodes on 2 of the 3 axes, explicitly excluding one axis to get a warning
    hp.place_nodes_on_axis(
        axis_id="A", unique_ids=["0", "1"], sorting_feature_to_use="a"
    )
    hp.place_nodes_on_axis(
        axis_id="B", unique_ids=["2", "3"], sorting_feature_to_use="b"
    )

    with pytest.warns() as record:
        viz_function(hp)
        plt.close()

    # confirm we get a single warning
    if viz_function == hive_plot_viz:
        assert (
            len(record) == 2
        ), f"Expected 2 warnings for no edges AND no nodes, got:\n{[i.message.args for i in record]}"
    else:
        assert (
            len(record) == 1
        ), f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"


def test_warnings_repeat_edge_kwarg() -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a warning when including a kwarg redundant to existing kwargs.

    (To warn the user that their kwarg will be ignored in deference to the existing kwarg.)
    """
    hp, edges = three_axis_hiveplot_example()
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B")
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C", a2_to_a1=False, lw=1)
    hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C", a1_to_a2=False)

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(instance=hp)

    with pytest.warns() as record:
        edge_viz(instance=hp, lw=2)

    assert (
        len(record) == 1
    ), f"Expected 1 warning, got:\n{[i.message.args for i in record]}"


@pytest.mark.parametrize("viz_function", [axes_viz, node_viz, edge_viz, p2cp_viz])
def test_viz_functions_run_p2cp(viz_function: callable) -> None:
    """
    Make sure the baseline viz functions can run without error when run for p2cps.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    p2cp = three_axis_p2cp_example()

    try:
        viz_function(p2cp)
        plt.close()
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail(f"`{viz_function}` should have run, but failed for some reason.")


@pytest.mark.parametrize("tags", [None, "two", ["two"], ["one", "three"]])
def test_p2cp_legend(tags: Union[str, list, None]) -> None:
    """
    Make sure the p2cp viz functions can run without error when run for p2cps.

    :param tags: which tags of data to add to the p2cp.
    """
    p2cp = three_axis_p2cp_example()
    p2cp.build_edges(tag="one")
    p2cp.build_edges(tag="two")
    p2cp.build_edges(tag="three", lw=0.5)
    fig, ax = p2cp_viz(p2cp, tags=tags)
    try:
        # make sure we can specify line kwargs or not
        p2cp_legend(p2cp, fig=fig, ax=ax, tags=tags, line_kwargs={"lw": 2})
        plt.close()
        p2cp_legend(p2cp, fig=fig, ax=ax, tags=tags)
        plt.close()
        assert True
    # fail test if not running for any reason
    except:  # noqa: E722
        traceback.print_exc()
        pytest.fail("`p2cp_legend` should have run, but failed for some reason.")


def test_warnings_repeat_edge_kwarg_p2cp() -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a warning when including a kwarg redundant to existing kwargs.

    (To warn the user that their kwarg will be ignored in deference to the existing kwarg.)
    """
    p2cp = three_axis_p2cp_example()
    p2cp.add_edge_kwargs(color="blue")

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(p2cp)

    with pytest.warns() as record:
        edge_viz(instance=p2cp, color="red")

    assert (
        len(record) == 1
    ), f"Expected 1 warning, got:\n{[i.message.args for i in record]}"


@pytest.mark.parametrize("viz_function", [axes_viz, label_axes, node_viz, edge_viz])
def test_expected_errors_wrong_types(viz_function: callable) -> None:
    """
    Viz functions should yell if tried on non-``P2CP`` or ``HivePlot`` instances.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    node = Node(unique_id=10)

    try:
        viz_function(node)
        plt.close()
        pytest.fail(
            f"`{viz_function}` should have failed on invalid input, but succeeded for some reason."
        )
    # supposed to fail
    except NotImplementedError:
        assert True


def test_warnings_node_viz_p2cp() -> None:
    """
    Make sure ``hiveplotlib.viz.node_viz()`` hits a single warning when nodes are unspecified.

    (If done correctly, this avoids sending a P2CP user a hive plot-specific internal warning that shouldn't
    matter. Even worse, if done wrong, the user would get the same warning *per axis* hence the check for a *single*
    warning.)
    """
    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    p2cp = P2CP(data=data)

    with pytest.warns() as record:
        node_viz(instance=p2cp)

    assert (
        len(record) == 1
    ), f"Expected 1 warning, got:\n{[i.message.args for i in record]}"

    # set the axes, then should be fine
    p2cp.set_axes(list(data.columns.values))

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        node_viz(p2cp)


@pytest.mark.parametrize("viz_function", [axes_viz, label_axes])
def test_warnings_no_axes_p2cp(viz_function: callable) -> None:
    """
    Make sure the baseline viz functions hits a warning when we are missing things when run for p2cps.

    :param viz_function: which function from ``hiveplotlib.viz`` to call.
    """
    p2cp = P2CP()

    with pytest.warns() as record:
        viz_function(p2cp)
        plt.close()

    # confirm we get a single warning
    assert (
        len(record) == 1
    ), f"Expected only 1 warning, got warnings of:\n{[i.message.args for i in record]}"


def test_label_axes_alignments() -> None:
    """
    Make sure ``hiveplotlib.viz.label_axes()`` does a "reasonable" alignment of axes labels for many angles.

    Note, this test exists primarily for two reasons: 1) test coverage and 2) a quick reference to make a scalable,
    multi-axis figure to observe all the alignments.
    """
    rng = np.random.default_rng(0)

    num_nodes = 50

    # number of axes on final p2cp
    num_axes = 26

    # axes names will be strings of integers 0, 1, 2, ...
    #  to make names longer (to see how alignment changes), repeat each string in each title this many times
    num_repeats_per_title = 5

    np_data = [np.sort(rng.uniform(low=0, high=10, size=num_nodes))] * num_axes
    data = pd.DataFrame(
        np.column_stack(np_data),
        columns=[i * num_repeats_per_title for i in np.arange(num_axes).astype(str)],
    )

    p2cp = P2CP(data=data)

    p2cp.set_axes(data.columns.values)

    try:
        axes_viz(p2cp)
        assert True
    except:  # noqa: E722
        pytest.fail("`axes_viz` should have run, but failed for some reason.")


def test_warnings_edge_viz_p2cp() -> None:
    """
    Make sure ``hiveplotlib.viz.edge_viz()`` hits a single warning when nodes are unspecified.

    (If done correctly, this avoids sending a P2CP user a hive plot-specific internal warning that shouldn't
    matter. Even worse, if done wrong, the user would get the same warning *per axis pair* hence the check for a
    *single* warning.)
    """
    rng = np.random.default_rng(0)

    num_nodes = 50

    # a and b columns move in sync with eventual node id (index), c is random
    data = pd.DataFrame(
        np.c_[
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
            rng.uniform(low=0, high=10, size=num_nodes),
        ],
        columns=["a", "b", "c"],
    )

    p2cp = P2CP(data=data)
    p2cp.set_axes(list(data.columns.values))

    with pytest.warns() as record:
        edge_viz(instance=p2cp)

    assert (
        len(record) == 1
    ), f"Expected 1 warning, got:\n{[i.message.args for i in record]}"

    # build the edges, then should be fine
    p2cp.build_edges()

    with warnings.catch_warnings():
        warnings.simplefilter("error")
        edge_viz(p2cp)
