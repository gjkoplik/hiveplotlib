# axis_test.py

"""
Tests for ``hiveplotlib.axis``.
"""

import numpy as np
import pytest
from hiveplotlib.axis import Axis

pytestmark = pytest.mark.unmarked


class TestAxis:
    """
    Tests for `Axis` class of `hiveplotlib`.
    """

    @pytest.mark.parametrize("angle", [0, 180])
    def test_positioning_example(self, angle: float) -> None:
        """
        Test initial positioning behaving as expected.

        :param angle: ccw angle off of positive x axis
        """
        start = 1
        end = 5
        axis = Axis(axis_id="axis0", start=start, end=end, angle=angle)

        # size should still be end - start
        assert np.linalg.norm(np.array(axis.start) - np.array(axis.end)) == end - start

        # angle of axis should be specified angle
        #  (take the dot product with [1, 0] and arccos to get the angle)
        assert (
            np.round(np.degrees(np.arccos(axis.start[0]))) == angle
        )  # round for numerical error

    def test_repr(self) -> None:
        """
        Make sure repr is as expected.
        """
        axis = Axis(axis_id="potato")
        assert "potato" in axis.__repr__()
