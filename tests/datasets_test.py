# datasets_test.py

"""
Tests for ``hiveplotlib.datasets.py``.
"""


from pathlib import Path

import numpy as np
import pytest
from hiveplotlib.datasets import (
    example_hive_plot,
    four_gaussian_blobs_3d,
    international_trade_data,
)

pytestmark = pytest.mark.unmarked


def test_example_hive_plot() -> None:
    """
    Sanity-check ``hiveplotlib.datasets.example_hive_plot()``.
    """
    num_nodes = 25
    num_edges = 50
    hp = example_hive_plot(num_nodes=num_nodes, num_edges=num_edges)

    assert len(hp.nodes) == num_nodes

    total_edges = 0
    for i in hp.edges:
        for j in hp.edges[i]:
            for k in hp.edges[i][j][0]["ids"]:
                total_edges += 1

    assert total_edges == num_edges


def test_four_gaussian_blobs_3d() -> None:
    """
    Sanity-check ``hiveplotlib.datasets.four_gaussian_blobs_3d()``.
    """
    df = four_gaussian_blobs_3d()

    assert df.shape == (200, 4)
    assert np.array_equal(np.unique(df.Label.values), np.arange(4))


def test_international_trade_data_good_example() -> None:
    """
    Make sure ``hiveplotlib.datasets.international_trade_data()`` works for the datasets that ship with ``hiveplotlib``.
    """
    data, metadata = international_trade_data(year=2019, hs92_code=8112)
    assert data.shape[0] > 0
    assert "citation" in metadata

    # can also test this with the path to the version-controlled files in hiveplotlib
    #  but only do this test if we're not in a post pypi CI job (e.g. if `./src/hiveplotlib` exists)
    if Path("./src/hiveplotlib").exists():
        data, metadata = international_trade_data(
            year=2019,
            hs92_code=8112,
            path="./src/hiveplotlib/datasets/trade_data_harvard_growth_lab",
        )
        assert data.shape[0] > 0
        assert "citation" in metadata


def test_international_trade_data_bad_example() -> None:
    """
    Make sure ``hiveplotlib.datasets.international_trade_data()`` fails for dataset not shipped with ``hiveplotlib``.
    """
    try:
        international_trade_data(year=1895, hs92_code=8112)
        pytest.fail("This should have failed for asking for non-existent dataset")
    except ValueError:
        assert True

    try:
        international_trade_data(year=2019, hs92_code=8112, path="./non-existent-path")
        pytest.fail("This should have failed for specifying wrong location")
    except ValueError:
        assert True
