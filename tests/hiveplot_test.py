# hiveplot_test.py

"""
Tests for ``hiveplotlib.HivePlot`` and ``hiveplotlib.hive_plot_n_axes``.
"""

import json
import warnings
from itertools import combinations
from typing import Tuple

import numpy as np
import pandas as pd
import pytest
from hiveplotlib import Axis, HivePlot, Node, hive_plot_n_axes
from hiveplotlib.datasets import example_hive_plot, example_nodes_and_edges
from hiveplotlib.utils import cartesian2polar, polar2cartesian

pytestmark = pytest.mark.unmarked


def three_axis_hiveplot_example() -> Tuple[HivePlot, np.ndarray]:
    """
    3 axis example for use in multiple tests later and simulated edges.

    (No edges added yet.)

    :return: ``HivePlot`` instance, (n, 2) numpy ndarray of edges (ids).
    """
    hp = HivePlot()

    # build nodes
    node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
    node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
    node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
    node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
    node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
    node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

    nodes = [node0, node1, node2, node3, node4, node5]

    hp.add_nodes(nodes)

    # build axes
    axis0 = Axis(axis_id="A", start=1, end=5, angle=0)
    axis1 = Axis(axis_id="B", start=1, end=5, angle=120)
    axis2 = Axis(axis_id="C", start=1, end=5, angle=240)

    axes = [axis0, axis1, axis2]

    hp.add_axes(axes)

    # hardcoded partition of nodes assignment for now
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        unique_ids=["0", "1"],
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        unique_ids=["2", "3"],
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="C",
        unique_ids=["4", "5"],
        sorting_feature_to_use="c",
        vmin=vmin,
        vmax=vmax,
    )

    # dummy of from, to pairs
    rng = np.random.default_rng(0)
    edges = rng.choice(np.arange(5).astype(str), size=100).reshape(-1, 2)

    return hp, edges


def two_axis_zero_crossing_hiveplot_example() -> Tuple[HivePlot, np.ndarray]:
    """
    2 axis example with axes on either side of 0 degrees.

    :return: ``HivePlot`` instance, (n, 2) numpy ndarray of edges (ids).
    """
    hp = HivePlot()

    # build nodes
    node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
    node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
    node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
    node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
    node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
    node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

    nodes = [node0, node1, node2, node3, node4, node5]

    hp.add_nodes(nodes)

    # build axes
    axis0 = Axis(axis_id="A", start=1, end=5, angle=20)
    axis1 = Axis(axis_id="B", start=1, end=5, angle=340)

    axes = [axis0, axis1]

    hp.add_axes(axes)

    # hardcoded partition of nodes assignment for now
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        unique_ids=["0", "1", "2"],
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        unique_ids=["3", "4", "5"],
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )

    # dummy of from, to pairs
    rng = np.random.default_rng(0)
    edges = rng.choice(np.arange(5).astype(str), size=100).reshape(-1, 2)

    return hp, edges


def repeated_nodes_hiveplot_example() -> Tuple[HivePlot, np.ndarray]:
    """
    2 axis example with some repeated nodes on either side of 0 degrees.

    :return: ``HivePlot`` instance, (n, 2) numpy ndarray of edges (ids).
    """
    hp = HivePlot()

    # build nodes
    node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
    node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
    node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
    node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
    node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
    node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

    nodes = [node0, node1, node2, node3, node4, node5]

    hp.add_nodes(nodes)

    # build axes
    axis0 = Axis(axis_id="A", start=1, end=5, angle=20)
    axis1 = Axis(axis_id="B", start=1, end=5, angle=340)

    axes = [axis0, axis1]

    hp.add_axes(axes)

    # hardcoded partition of nodes with overlap
    vmin = 0
    vmax = 10
    hp.place_nodes_on_axis(
        axis_id="A",
        unique_ids=["0", "1", "2", "3"],
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        unique_ids=["2", "3", "4", "5"],
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )

    # dummy of from, to pairs
    rng = np.random.default_rng(0)
    edges = rng.choice(np.arange(5).astype(str), size=100).reshape(-1, 2)

    return hp, edges


def repeat_axis_hiveplot_example() -> Tuple[HivePlot, np.ndarray]:
    """
    Hive plot example with 2 axes, 1 a repeat of the other.

    :return: ``HivePlot`` instance, (n, 2) numpy ndarray of edges (ids).
    """
    hp = HivePlot()

    # build axes

    axis0 = Axis(axis_id="A", start=1, end=5, angle=40)
    axis1 = Axis(axis_id="B", start=1, end=5, angle=320)

    axes = [axis0, axis1]

    hp.add_axes(axes)

    # build nodes

    num_nodes = 100
    rng = np.random.default_rng(0)

    nodes = []
    for i in range(num_nodes):
        temp_node = Node(
            unique_id=str(i),
            data={"a": rng.uniform(), "b": rng.uniform(), "c": rng.uniform()},
        )
        nodes.append(temp_node)
    hp.add_nodes(nodes)

    node_ids = list(np.arange(num_nodes).astype(str))

    # repeated nodes
    vmin = 0
    vmax = 1
    hp.place_nodes_on_axis(
        axis_id="A",
        unique_ids=node_ids,
        sorting_feature_to_use="a",
        vmin=vmin,
        vmax=vmax,
    )
    hp.place_nodes_on_axis(
        axis_id="B",
        unique_ids=node_ids,
        sorting_feature_to_use="b",
        vmin=vmin,
        vmax=vmax,
    )

    # dummy of from, to pairs
    num_edges = 10
    edges = rng.choice(np.arange(num_nodes).astype(str), size=num_edges * 2).reshape(
        -1, 2
    )

    return hp, edges


class TestHivePlot:
    """
    Tests for hiveplotlib.HivePlot.
    """

    def test_add_axes_example(self) -> None:
        """
        Check that ``HivePlot.add_axes()`` behaves as expected on a "nice" example.
        """
        axis0 = Axis(axis_id="a0", angle=10)
        axis1 = Axis(axis_id="a1", angle=30)

        hp = HivePlot()
        hp.add_axes([axis0, axis1])

        assert hp.axes["a0"].angle == 10
        assert hp.axes["a1"].angle == 30

    def test_add_axes_hostile_input(self) -> None:
        """
        Check that ``HivePlot.add_axes()`` fails when adding redundant IDs.
        """
        axis0 = Axis(axis_id="a0", angle=10)
        axis1 = Axis(axis_id="a1", angle=20)
        axis2 = Axis(axis_id="a0", angle=30)

        hp = HivePlot()
        try:
            hp.add_axes([axis0, axis1, axis2])
            raise ValueError
        except AssertionError:
            assert True

    def test_add_axes_hostile_overlap(self) -> None:
        """
        Check that ``HivePlot.add_axes()`` fails when IDs redundant with existing IDs.
        """
        axis0 = Axis(axis_id="a0", angle=10)
        axis1 = Axis(axis_id="a1", angle=20)
        axis2 = Axis(axis_id="a0", angle=30)

        hp = HivePlot()
        hp.add_axes([axis0, axis1])
        try:
            hp.add_axes([axis2])
            raise ValueError
        except AssertionError:
            assert True

    def test_add_nodes_example(self) -> None:
        """
        Check that ``HivePlot.add_nodes()`` behaves as expected on a "nice" example.
        """
        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n2", data={"a": 2})

        hp = HivePlot()
        hp.add_nodes(nodes=[node0, node1, node2])

        for i in range(3):
            assert hp.nodes[f"n{i}"].data["a"] == i

    def test_add_nodes_hostile_input(self) -> None:
        """
        Check that ``HivePlot.add_nodes()`` fails when adding redundant IDs.
        """
        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n0", data={"a": 2})

        hp = HivePlot()
        try:
            hp.add_nodes([node0, node1, node2])
            raise ValueError
        except AssertionError:
            assert True

    def test_add_nodes_hostile_overlap(self) -> None:
        """
        Check that ``HivePlot.add_nodes()`` fails when IDs redundant with existing IDs.
        """
        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n0", data={"a": 2})

        hp = HivePlot()
        hp.add_nodes([node0, node1])
        try:
            hp.add_nodes([node2])
            raise ValueError
        except AssertionError:
            assert True

    def test__allocate_nodes_to_axis_example(self) -> None:
        """
        Check that ``HivePlot._allocate_nodes_to_axis()`` behaves as expected with a "nice" example.
        """
        axis0 = Axis(axis_id="a0", angle=10)
        axis1 = Axis(axis_id="a1", angle=20)
        axis2 = Axis(axis_id="a2", angle=30)
        axes = [axis0, axis1, axis2]

        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n2", data={"a": 2})
        nodes = [node0, node1, node2]

        hp = HivePlot()
        hp.add_nodes(nodes=nodes)
        hp.add_axes(axes=axes)

        hp._allocate_nodes_to_axis(unique_ids=["n0", "n1"], axis_id="a0")
        hp._allocate_nodes_to_axis(unique_ids=["n1", "n2"], axis_id="a1")
        hp._allocate_nodes_to_axis(unique_ids=["n2", "n0"], axis_id="a2")

        assert hp.node_assignments == {
            "a0": ["n0", "n1"],
            "a1": ["n1", "n2"],
            "a2": ["n2", "n0"],
        }

    @pytest.mark.parametrize("angle", [0, 120])
    def test_place_nodes_on_axis_example(self, angle: float) -> None:
        """
        Check that ``HivePlot.place_nodes_on_axis()`` behaves as expected with a "nice" example.

        :param angle: angle of ``Axis`` instance.
        """
        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n2", data={"a": 2})
        nodes = [node0, node1, node2]

        start = 1
        end = 5
        axis0 = Axis(axis_id="a0", start=start, end=end, angle=angle)

        hp = HivePlot()
        hp.add_nodes(nodes=nodes)
        hp.add_axes(axes=[axis0])

        hp.place_nodes_on_axis(
            axis_id="a0",
            unique_ids=["n0", "n1", "n2"],
            sorting_feature_to_use="a",
            vmin=None,
            vmax=None,
        )

        # should have 3 points in total placed on the axis
        df = hp.axes["a0"].node_placements
        assert df.shape[0] == 3
        assert np.array_equal(
            df.loc[:, "unique_id"].values, np.array(["n0", "n1", "n2"])
        )

        # n0 at start of axis
        assert np.allclose(
            df.loc[df.loc[:, "unique_id"] == "n0", ["x", "y"]].values,
            np.array(polar2cartesian(rho=start, phi=angle)),
        )

        # n1 at midpoint of axis
        assert np.allclose(
            df.loc[df.loc[:, "unique_id"] == "n1", ["x", "y"]].values,
            np.array(polar2cartesian(rho=(start + end) / 2, phi=angle)),
        )

        # n2 at end of axis
        assert np.allclose(
            df.loc[df.loc[:, "unique_id"] == "n2", ["x", "y"]].values,
            np.array(polar2cartesian(rho=end, phi=angle)),
        )

    @pytest.mark.parametrize("angle", [0, 120])
    @pytest.mark.parametrize("vmin_vmax", [[-1, 3], [1, 3], [1, 1.5], [-6, -5], [5, 6]])
    def test_place_nodes_on_axis_change_vmin_vmax(
        self, angle: float, vmin_vmax: list
    ) -> None:
        """
        Check that ``HivePlot.place_nodes_on_axis()`` correctly shifts node placement when changing vmin and vmax.

        :param angle: angle of ``Axis`` instance.
        :param vmin_vmax: vmin and vmax values to use.
        """
        # force a rescaling on both sides
        vmin = vmin_vmax[0]
        vmax = vmin_vmax[1]

        node0 = Node(unique_id="n0", data={"a": 0})
        node1 = Node(unique_id="n1", data={"a": 1})
        node2 = Node(unique_id="n2", data={"a": 2})
        nodes = [node0, node1, node2]

        start = 1
        end = 5
        axis0 = Axis(axis_id="a0", start=start, end=end, angle=angle)

        hp = HivePlot()
        hp.add_nodes(nodes=nodes)
        hp.add_axes(axes=[axis0])

        hp.place_nodes_on_axis(
            axis_id="a0",
            unique_ids=["n0", "n1", "n2"],
            sorting_feature_to_use="a",
            vmin=vmin,
            vmax=vmax,
        )

        # should have 3 points in total placed on the axis
        df = hp.axes["a0"].node_placements
        assert df.shape[0] == 3
        assert np.array_equal(
            df.loc[:, "unique_id"].values, np.array(["n0", "n1", "n2"])
        )

        # scaled positions check
        for node_id in ["n0", "n1", "n2"]:
            node_val = hp.nodes[node_id].data["a"]
            # scale to [vmin, vmax] dim
            node_val = np.minimum(node_val, vmax)
            node_val = np.maximum(node_val, vmin)
            # scale to vmin = 0, vmax = 1
            scaled_position = (node_val - vmin) / (vmax - vmin)
            # rescale to size and shift of real axes
            new_rho = scaled_position * (end - start) + start
            assert np.allclose(
                df.loc[df.loc[:, "unique_id"] == node_id, ["x", "y"]].values,
                np.array(polar2cartesian(rho=new_rho, phi=angle)),
            )

    @pytest.mark.parametrize("angle", [0, 120])
    def test_place_nodes_on_axis_one_point(self, angle: float) -> None:
        """
        Check that ``HivePlot.place_nodes_on_axis()`` correctly places a single point.

        Specifically, that the point is placed at the midpoint of an axis when no vmin or vmax are specified.

        :param angle: angle of ``Axis`` instance.
        """
        node0 = Node(unique_id="n0", data={"a": 0})
        nodes = [node0]

        start = 1
        end = 5
        axis0 = Axis(axis_id="a0", start=start, end=end, angle=angle)

        hp = HivePlot()
        hp.add_nodes(nodes=nodes)
        hp.add_axes(axes=[axis0])

        hp.place_nodes_on_axis(
            axis_id="a0",
            unique_ids=["n0"],
            sorting_feature_to_use="a",
            vmin=None,
            vmax=None,
        )

        # should have 3 points in total placed on the axis
        df = hp.axes["a0"].node_placements
        assert df.shape[0] == 1
        assert np.array_equal(df.loc[:, "unique_id"].values, np.array(["n0"]))

        # n0 at midpoint of axis
        assert np.allclose(
            df.loc[df.loc[:, "unique_id"] == "n0", ["x", "y"]].values,
            np.array(polar2cartesian(rho=(start + end) / 2, phi=angle)),
        )

    def test_place_nodes_on_axis_resetting_curves(self) -> None:
        """
        Check that ``HivePlot.place_nodes_on_axis()`` kills any existing Bezier curves that connect to that axis.

        (The logic being, if you place the nodes, presumably they've been moved to different locations. Thus, one
        would need to redraw the Bezier curves (or at least for this method, one would not want to plot old, unrelated
        Bezier curves).
        """
        hp, edges = three_axis_hiveplot_example()

        # connect some of the edges
        a_b_tag = hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
        b_c_tag = hp.connect_axes(edges=edges, axis_id_1="B", axis_id_2="C")

        # should have real curves for A <-> B and B <-> C  connections
        assert (
            hp.edges["A"]["B"][a_b_tag]["curves"].size > 0
        ), "Should be non-empty numpy array of Bezier curves at this point."
        assert (
            hp.edges["B"]["A"][a_b_tag]["curves"].size > 0
        ), "Should be non-empty numpy array of Bezier curves at this point."
        assert (
            hp.edges["C"]["B"][b_c_tag]["curves"].size > 0
        ), "Should be non-empty numpy array of Bezier curves at this point."
        assert (
            hp.edges["B"]["C"][b_c_tag]["curves"].size > 0
        ), "Should be non-empty numpy array of Bezier curves at this point."

        # re-placing A's nodes with a new sorting procedure should kill any A <-> B curves but not and B <-> C
        hp.place_nodes_on_axis(axis_id="A", sorting_feature_to_use="b")

        assert (
            "curves" not in hp.edges["A"]["B"][a_b_tag]
        ), "Bezier curves between 'A' -> 'B' should have been dropped when re-placing nodes."
        assert (
            "curves" not in hp.edges["B"]["A"][a_b_tag]
        ), "Bezier curves between 'A' <- 'B' should have been dropped when re-placing nodes."
        assert (
            hp.edges["B"]["C"][b_c_tag]["curves"].size > 0
        ), "Bezier curves between 'B' and 'C' should be unaffected by messing with 'A'."

    @pytest.mark.parametrize("a1_to_a2", [True, False])
    @pytest.mark.parametrize("a2_to_a1", [True, False])
    def test_add_edge_ids_example(self, a1_to_a2: bool, a2_to_a1: bool) -> None:
        """
        Test ``HivePlot.add_edge_ids()`` correctly assigns edges with friendly example.

        :param a1_to_a2: whether to add edges from the first to second axis.
        :param a2_to_a1: whether to add edges from the second to first axis.
        """
        hp, edges = three_axis_hiveplot_example()

        edge_combinations = list(combinations(hp.axes.keys(), 2))

        # starting with no edges
        assert hp.edges == {}

        for source, sink in edge_combinations:
            # testing appropriate key existence

            if a1_to_a2:
                # the [source][sink] key should not exist before
                try:
                    assert source not in hp.edges
                except AssertionError:
                    assert sink not in hp.edges[source]

            if a2_to_a1:
                # the [sink][source] key should not exist before
                try:
                    assert sink not in hp.edges
                except AssertionError:
                    assert source not in hp.edges[sink]

            # appropriate keys should now exist after
            try:
                tag = hp.add_edge_ids(
                    edges=edges,
                    axis_id_1=source,
                    axis_id_2=sink,
                    a1_to_a2=a1_to_a2,
                    a2_to_a1=a2_to_a1,
                )
            except ValueError:
                if not a1_to_a2 and not a2_to_a1:
                    assert True
                else:
                    pytest.fail(
                        f"Should not trigger a ValueError with a1_to_a2={a1_to_a2} and a2_to_a1={a2_to_a1}"
                    )

            if a1_to_a2:
                assert sink in hp.edges[source]
            if a2_to_a1:
                assert source in hp.edges[sink]

            # testing appropriate edge existence

            if a1_to_a2:
                # edges in source (first) column should all be in source axis
                assert np.isin(
                    hp.edges[source][sink][tag]["ids"][:, 0],
                    hp.axes[source].node_placements.unique_id.values,
                ).all()

                # edges in sink (second) column should all be in sink axis
                assert np.isin(
                    hp.edges[source][sink][tag]["ids"][:, 1],
                    hp.axes[sink].node_placements.unique_id.values,
                ).all()

            if a2_to_a1:
                # edges in first column should all be in source axis
                assert np.isin(
                    hp.edges[sink][source][tag]["ids"][:, 0],
                    hp.axes[sink].node_placements.unique_id.values,
                ).all()

                # edges in second column should all be in sink axis
                assert np.isin(
                    hp.edges[sink][source][tag]["ids"][:, 1],
                    hp.axes[source].node_placements.unique_id.values,
                ).all()

    @pytest.mark.parametrize(
        "hp_function",
        [
            three_axis_hiveplot_example,
            two_axis_zero_crossing_hiveplot_example,
            repeated_nodes_hiveplot_example,
            repeat_axis_hiveplot_example,
        ],
    )
    @pytest.mark.parametrize("short_arc", [True, False])
    def test_add_edge_curves_between_axes_example(
        self, hp_function: callable, short_arc: bool
    ) -> None:
        """
        Test that ``HivePlot.add_edge_curves_between_axes()`` correctly builds our edge curves with friendly examples.

        Do this by checking the polar angle of the discretized curves.

        :param hp_function: which graph-generating function to use.
        :param short_arc: whether to take the minimum angle arc or not when drawing edges.
        """
        hp, edges = hp_function()
        edge_combinations = list(combinations(hp.axes.keys(), 2))

        num_steps = 100

        for a0, a1 in edge_combinations:
            tag = hp.add_edge_ids(edges=edges, axis_id_1=a0, axis_id_2=a1)
            hp.add_edge_curves_between_axes(
                axis_id_1=a0,
                axis_id_2=a1,
                num_steps=num_steps,
                tag=tag,
                short_arc=short_arc,
            )

            # check the arcs in both directions
            for [i, j] in [[a0, a1], [a1, a0]]:
                curves = hp.edges[i][j][tag]["curves"].copy().astype(float)
                # drop the spacer values
                curves = curves[~np.isnan(curves)].reshape(-1, 2)

                # get polar coordinates
                rho, phi = cartesian2polar(x=curves[:, 0], y=curves[:, 1])

                # distance from origin should be smaller than the largest two rho values for the two connecting points
                #  (it can be smaller than the minimum)
                ids = hp.edges[i][j][tag]["ids"].copy()
                for k in range(ids.shape[0]):
                    # curve from and two node IDs
                    node_id_i = ids[k, 0]
                    node_id_j = ids[k, 1]

                    # rho values for those two nodes
                    rho_i = (
                        hp.axes[i]
                        .node_placements.set_index("unique_id")
                        .loc[node_id_i, "rho"]
                    )
                    rho_j = (
                        hp.axes[j]
                        .node_placements.set_index("unique_id")
                        .loc[node_id_j, "rho"]
                    )

                    # the curve of interest (drop the np.nan at the end)
                    temp_curve = rho[k * num_steps : (k + 1) * num_steps]
                    assert np.round(temp_curve.max(), 4) <= np.round(
                        np.maximum(rho_i, rho_j), 4
                    ), (
                        "rho value rose above expected maximum (e.g. curve got farther from origin than expected\n"
                        f"From node {node_id_i} to node {node_id_j}\n"
                        f"rho_i {rho_i} and rho_j {rho_j}\n"
                        f"From axis {i} to axis {j}"
                    )

                # for angle expectations, we will use the definition of dot product to get the angle with the
                #  starting axis
                # use dot product definition on the discretized curves against each axis and arccos the angle
                #  no discretized point should be more than `min angle` degrees away from an axis
                start = hp.axes[i].start
                dotted = curves[:, 0] * start[0] + curves[:, 1] * start[1]
                # rounding to avoid numerical error
                normalized = np.round(
                    dotted / (np.linalg.norm(start) * np.linalg.norm(curves, axis=1)), 5
                )
                angles = np.round(np.degrees(np.arccos(normalized)), 2)

                # if we take the short arc, angle should be bounded by the minimum angle between two axes
                #  and monotonically increasing or decreasing
                if short_arc:
                    # monotonicity check for each curve
                    for k in range(ids.shape[0]):
                        temp_angles = angles[k * num_steps : (k + 1) * num_steps]
                        diff = np.diff(temp_angles) > 0
                        # either all increasing or all decreasing
                        assert diff.sum() == diff.size or diff.sum() == 0, (
                            "Angle must be monotonic diff, since there should not be a 180 degree "
                            + "crossing of the curve\n"
                            + f"From node {ids[k, 0]} to node {ids[k, 1]}\n"
                            + f"From axis {i} to axis {j}\n"
                            + f"From angle {hp.axes[i].angle} to angle {hp.axes[j].angle}\n"
                            + f"angles:\n {temp_angles}\n"
                            + f"diff > 0:\n {diff}"
                        )

                    # bound check for angles on curves
                    angle_diff = np.abs(hp.axes[i].angle - hp.axes[j].angle)
                    if angle_diff > 180:
                        angle_diff = 360 - angle_diff
                    assert angles.max() <= angle_diff, (
                        "Maximum angle from start axis should not exceed the minimum angle between the two axes\n"
                        + f"From axis {i} to axis {j}"
                    )

                # otherwise, we guarantee an angle greater than 180 degrees
                #  dot product always returns the minimum angle, so this can't be seen
                #  BUT, passing 180 degrees will trigger a lack of monotonicity in the angles that would be
                #  otherwise impossible
                else:
                    # must have both increasing and decreasing diff
                    for k in range(ids.shape[0]):
                        temp_angles = angles[k * num_steps : (k + 1) * num_steps]
                        diff = np.diff(temp_angles) > 0
                        assert diff.sum() < diff.size, (
                            "Must be non-monotonic diff, or there was no 180 degree crossing of the curve\n"
                            + f"From node {ids[k, 0]} to node {ids[k, 1]}\n"
                            + f"From axis {i} to axis {j}\n"
                            + f"From angle {hp.axes[i].angle} to angle {hp.axes[j].angle}\n"
                            + f"angles:\n {temp_angles}\n"
                            + f"diff > 0:\n {diff}"
                        )

    def test_construct_curves(self) -> None:
        """
        Test that ``HivePlot.construct_curves()`` correctly builds the correctly needed curves.

        (e.g. edges whose "ids" are specified but not yet "curves".)
        """
        hp, edges = three_axis_hiveplot_example()

        # only add the IDs, not edges or kwargs
        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")

        # curves should not exist yet
        assert (
            "curves" not in hp.edges["A"]["B"][tag]
        ), "Bezier curves between 'A' -> 'B' should not exist yet."
        assert (
            "curves" not in hp.edges["B"]["A"][tag]
        ), "Bezier curves between 'A' <- 'B' should not exist yet."

        # add curves for all specified IDs
        hp.construct_curves()

        # should have real curves for the above A <-> B connections, but nothing else
        assert (
            hp.edges["A"]["B"][tag]["curves"].size > 0
        ), "Should be non-empty numpy array of Bezier curves between 'A' -> 'B' at this point."
        assert (
            hp.edges["B"]["A"][tag]["curves"].size > 0
        ), "Should be non-empty numpy array of Bezier curves between 'A' <- 'B' at this point."

        assert (
            "C" not in hp.edges["A"]
        ), "These IDs or edges should not been defined yet"

    def test_add_edge_kwargs(self) -> None:
        """
        Test that ``HivePlot.add_edge_kwargs()`` correctly adds kwargs where specified.
        """
        hp, edges = three_axis_hiveplot_example()

        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")

        hp.add_edge_kwargs(
            axis_id_1="A", axis_id_2="B", a2_to_a1=False, tag=tag, c="C0"
        )

        # should only have generated "A" -> "B" edge_kwargs
        assert (
            hp.edges["A"]["B"][tag]["edge_kwargs"] == {"c": "C0"}
        ), "Should have generated a simple dict of color for edge kwargs going from 'A' -> 'B'."
        assert (
            "edge_kwargs" not in hp.edges["B"]["A"][tag]
        ), "Should not have any edge kwargs going from 'B' -> 'A'."

    def test_connect_axes(self) -> None:
        """
        Test that ``HivePlot.connect_axes()`` correctly builds edge id connections, Bezier curves, and specifies kwargs.
        """
        hp, edges = three_axis_hiveplot_example()

        tag = hp.connect_axes(
            edges=edges, axis_id_1="A", axis_id_2="B", a2_to_a1=False, c="C0"
        )

        # should only have generated "A" -> "B" ids, curves, and edge_kwargs
        assert (
            hp.edges["A"]["B"][tag]["ids"].size > 0
        ), "Should be non-empty numpy array of node IDs representing edges between 'A' -> 'B' at this point."
        assert (
            hp.edges["A"]["B"][tag]["curves"].size > 0
        ), "Should be non-empty numpy array of Bezier curves between 'A' -> 'B' at this point."
        assert (
            hp.edges["A"]["B"][tag]["edge_kwargs"] == {"c": "C0"}
        ), "Should have generated a simple dict of color for edge kwargs going from 'A' -> 'B'."

        assert (
            "B" not in hp.edges
        ), "Should not have generated any edge information between 'B' to 'A' at this point."

    def test_hive_plot_n_axes_example(self) -> None:
        """
        Test that ``hive_plot_n_axes()`` builds the ``HivePlot`` instance we expect with a friendly example.
        """
        # generate synthetic node data from a pandas dataframe
        num_nodes = 50
        rng = np.random.default_rng(0)
        data = pd.DataFrame(
            np.c_[
                rng.uniform(low=0, high=10, size=num_nodes),
                rng.uniform(low=10, high=20, size=num_nodes),
                rng.uniform(low=20, high=30, size=num_nodes),
            ],
            columns=["low", "med", "high"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        # generate random edges
        num_edges = 200

        edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        hp = hive_plot_n_axes(
            node_list=nodes,
            edges=edges,
            axes_assignments=[
                np.arange(num_nodes)[: num_nodes // 3],
                np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                np.arange(num_nodes)[2 * num_nodes // 3 :],
            ],
            sorting_variables=["low", "med", "high"],
            axes_names=["Low", "Medium", "High"],
            repeat_axes=[False, True, False],
            vmins=[0, 0, 0],
            vmaxes=[30, 30, 30],
        )

        assert sorted(hp.axes.keys()) == [
            "High",
            "Low",
            "Medium",
            "Medium_repeat",
        ], "Should have only generated a repeat Medium axis."

        assert (
            hp.axes["Low"].node_placements["rho"].mean()
            < hp.axes["Medium"].node_placements["rho"].mean()
        ), "by construction, rho values for 'Low' axis should be smaller than for 'Medium' axis."

        assert (
            hp.axes["Medium"].node_placements["rho"].mean()
            < hp.axes["High"].node_placements["rho"].mean()
        ), "by construction, rho values for 'Medium' axis should be smaller than for 'High' axis."

        # we should have curves between these axes
        valid_curves = [
            ["Low", "Medium"],
            ["Medium", "Low"],
            ["Medium", "Medium_repeat"],
            ["Medium_repeat", "High"],
            ["High", "Medium_repeat"],
            ["High", "Low"],
            ["Low", "High"],
        ]
        for a0, a1 in valid_curves:
            assert (
                list(hp.edges[a0][a1].keys()) == [0]
            ), "All edge adds are unique, no unique tag specified, so all tags should be 0"
            assert (
                "curves" in hp.edges[a0][a1][0]
            ), f"Expected Bezier curves calculated from {a0} to {a1} but did not find them."

        assert (
            "Medium" not in hp.edges["Medium_repeat"]
        ), "Did not expect any connections between 'Medium_repeat' and 'Medium' but found them."

    def test_hive_plot_n_axes_with_none_axis_assignment(self) -> None:
        """
        Confirm ``hive_plot_n_axes()`` will run with a ``None`` for one of the axis assignments in ``axes_assignments``.
        """
        node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
        node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
        node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
        node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
        node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
        node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

        nodes = [node0, node1, node2, node3, node4, node5]

        # dummy of from, to pairs
        rng = np.random.default_rng(0)
        edges = rng.choice(np.arange(len(nodes)).astype(str), size=100).reshape(-1, 2)

        hp_normal = hive_plot_n_axes(
            node_list=nodes,
            edges=edges,
            axes_assignments=[["0", "1"], ["2", "3"], ["4", "5"]],
            sorting_variables=["a"] * 3,
            axes_names=["_", "__", "to_check"],
        )

        hp_with_none = hive_plot_n_axes(
            node_list=nodes,
            edges=edges,
            axes_assignments=[["0", "1"], ["2", "3"], None],
            sorting_variables=["a"] * 3,
            axes_names=["_", "__", "to_check"],
        )

        hp_with_double_none = hive_plot_n_axes(
            node_list=nodes,
            edges=edges,
            axes_assignments=[["0", "1", "2", "3"], None, None],
            sorting_variables=["a"] * 3,
            axes_names=["_", "also_to_check", "to_check"],
        )

        node_placements_normal = hp_normal.axes["to_check"].node_placements.sort_values(
            "unique_id"
        )
        normal_unique_ids = list(node_placements_normal.unique_id.values)
        normal_rho_values = list(node_placements_normal.rho.values)

        node_placements_with_none = hp_with_none.axes[
            "to_check"
        ].node_placements.sort_values("unique_id")
        with_none_unique_ids = list(node_placements_with_none.unique_id.values)
        with_none_rho_values = list(node_placements_with_none.rho.values)

        node_placements_with_double_none = hp_with_double_none.axes[
            "to_check"
        ].node_placements.sort_values("unique_id")
        with_double_none_unique_ids = list(
            node_placements_with_double_none.unique_id.values
        )
        with_double_none_rho_values = list(node_placements_with_double_none.rho.values)

        assert (
            normal_unique_ids == with_none_unique_ids == with_double_none_unique_ids
        ), "Should have chosen the same unique IDs"

        assert (
            normal_rho_values == with_none_rho_values == with_double_none_rho_values
        ), "Should have equivalent rho values"

    def test_hive_plot_n_axes_run_with_unspecified_params(self) -> None:
        """
        Confirm ``hive_plot_n_axes()`` will run with unspecified kwargs (e.g. call the defaults).
        """
        # generate synthetic node data from a pandas dataframe
        num_nodes = 50
        rng = np.random.default_rng(0)
        data = pd.DataFrame(
            np.c_[
                rng.uniform(low=0, high=10, size=num_nodes),
                rng.uniform(low=10, high=20, size=num_nodes),
                rng.uniform(low=20, high=30, size=num_nodes),
            ],
            columns=["low", "med", "high"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        # generate random edges
        num_edges = 200

        edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        hp = hive_plot_n_axes(
            node_list=nodes,
            edges=edges,
            axes_assignments=[
                np.arange(num_nodes)[: num_nodes // 3],
                np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                np.arange(num_nodes)[2 * num_nodes // 3 :],
            ],
            sorting_variables=["low", "med", "high"],
        )

        # default no repeat axes
        assert len(list(hp.axes.keys())) == 3, "Default no repeat axes."

        # default names
        assert sorted(hp.axes.keys()) == [
            "Group 1",
            "Group 2",
            "Group 3",
        ], "Default Names."

        # default placements spanning each axis
        for axis_id in hp.axes:
            assert (
                hp.axes[axis_id].node_placements.loc[:, "rho"].min() == 1
            ), f"Minimum node placement for axis {axis_id} should be 1 (min location by construction) for default."
            assert (
                hp.axes[axis_id].node_placements.loc[:, "rho"].max() == 5
            ), f"Maximum node placement for axis {axis_id} should be 5 (max location by construction) for default."

    @pytest.mark.parametrize("angle_between_repeat_axes", [40, 120, 140])
    def test_hive_plot_n_axes_repeat_axes_crossing_other_axes(
        self, angle_between_repeat_axes: float
    ) -> None:
        """
        Confirm ``hive_plot_n_axes()`` will warn when repeat axes will touch or cross other axes.

        :param angle_between_repeat_axes: angle used in between repeat axes.
        """
        # generate synthetic node data from a pandas dataframe
        num_nodes = 50
        rng = np.random.default_rng(0)
        data = pd.DataFrame(
            np.c_[
                rng.uniform(low=0, high=10, size=num_nodes),
                rng.uniform(low=10, high=20, size=num_nodes),
                rng.uniform(low=20, high=30, size=num_nodes),
            ],
            columns=["low", "med", "high"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        # generate random edges
        num_edges = 200

        edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        # doing 3 axes so 120 degrees is the number to worry about
        if angle_between_repeat_axes >= 120:
            with pytest.warns() as record:
                hive_plot_n_axes(
                    node_list=nodes,
                    edges=edges,
                    axes_assignments=[
                        np.arange(num_nodes)[: num_nodes // 3],
                        np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                        np.arange(num_nodes)[2 * num_nodes // 3 :],
                    ],
                    sorting_variables=["low", "med", "high"],
                    angle_between_repeat_axes=angle_between_repeat_axes,
                )

            assert (
                len(record) == 1
            ), f"Expected 1 warning for edge angle, got:\n{[i.message.args for i in record]}"

        else:
            with warnings.catch_warnings():
                warnings.simplefilter("error")
                hive_plot_n_axes(
                    node_list=nodes,
                    edges=edges,
                    axes_assignments=[
                        np.arange(num_nodes)[: num_nodes // 3],
                        np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                        np.arange(num_nodes)[2 * num_nodes // 3 :],
                    ],
                    sorting_variables=["low", "med", "high"],
                    angle_between_repeat_axes=angle_between_repeat_axes,
                )

    def test_hashable_unique_ids(self) -> None:
        """
        Make sure we can pass around hashable ids and things will work.
        """
        node_list = [
            Node(unique_id=(i, j), data={i: i, j: j})
            for i in range(5)
            for j in range(5)
        ]

        axis = Axis(axis_id=("a", "b"))

        hp = HivePlot()
        hp.add_nodes(node_list)
        hp.add_axes([axis])

        assert list(hp.axes.keys()) == [("a", "b")]

        # make sure nodes were placed without issue
        hp.place_nodes_on_axis(
            axis_id=("a", "b"),
            unique_ids=[(0, j) for j in range(5)],
            sorting_feature_to_use=0,
        )

        assert hp.axes[("a", "b")].node_placements.shape[0] == 5

    def test_no_specified_node_placement_label(self) -> None:
        """
        Confirm we can't place nodes on an axis in a ``HivePlot`` without specifying a node placement label.

        Specifically, placing the node placement label before or during the placement call.
        """
        hp = HivePlot()

        # build nodes
        node0 = Node(unique_id="0", data={"a": 1, "b": 2, "c": 3})
        node1 = Node(unique_id="1", data={"a": 2, "b": 3, "c": 4})
        node2 = Node(unique_id="2", data={"a": 3, "b": 4, "c": 5})
        node3 = Node(unique_id="3", data={"a": 4, "b": 5, "c": 6})
        node4 = Node(unique_id="4", data={"a": 5, "b": 6, "c": 7})
        node5 = Node(unique_id="5", data={"a": 6, "b": 7, "c": 8})

        nodes = [node0, node1, node2, node3, node4, node5]

        hp.add_nodes(nodes)

        # build axes
        axis0 = Axis(axis_id="A", start=1, end=5, angle=0)
        axis1 = Axis(axis_id="B", start=1, end=5, angle=120)
        axis2 = Axis(axis_id="C", start=1, end=5, angle=240)

        axes = [axis0, axis1, axis2]

        hp.add_axes(axes)

        # hardcoded partition of nodes assignment for now
        vmin = 0
        vmax = 10
        try:
            hp.place_nodes_on_axis(
                axis_id="A",
                unique_ids=["0", "1"],
                sorting_feature_to_use=None,
                vmin=vmin,
                vmax=vmax,
            )
            raise ValueError(
                "Should have triggered an AssertionError for not having a sorting"
                + "procedure for the axis ever."
            )
        except AssertionError:
            assert True

    @pytest.mark.parametrize(
        "reset",
        [
            "tag_a1_to_a2_only",
            "tag_a2_to_a1_only",
            "tag_bidirectional",
            "axis_pair",
            "axis_pair_one_direction",
            "entire_from_axis",
            "entire_to_axis",
            "entire_axis",
            "all_edges",
            "invalid",
            "tag_not_found",
        ],
    )
    def test_reset_edges(self, reset: str) -> None:
        """
        Confirm we can kill any and all edge connections, curves, and kwargs in with ``HivePlot.reset_edges()``.

        :param reset: which method of edge reset to consider.
        """
        hp, edges = three_axis_hiveplot_example()

        # connect some edges
        tag = hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
        tag1 = hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B")
        hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="C")
        # throw in an edge kwarg for coverage
        hp.connect_axes(edges=edges, axis_id_1="C", axis_id_2="B", c="C0")

        assert (
            len(list(hp.edges.keys())) > 0
        ), "Should have edge information at this point."

        if reset == "tag_a1_to_a2_only":
            hp.reset_edges(axis_id_1="A", axis_id_2="B", tag=tag, a2_to_a1=False)
            assert tag not in hp.edges["A"]["B"], "(A, B, tag) edges should've dropped."
            assert (
                tag1 in hp.edges["A"]["B"]
            ), "(A, B, tag1) edges should've maintained."
            assert tag in hp.edges["B"]["A"], "(B, A, tag) edges should've maintained."
            assert tag in hp.edges["A"]["C"]
            assert tag in hp.edges["C"]["A"]
            assert tag in hp.edges["B"]["C"]
            assert tag in hp.edges["C"]["B"]

        elif reset == "tag_a2_to_a1_only":
            hp.reset_edges(axis_id_1="A", axis_id_2="B", tag=tag, a1_to_a2=False)
            assert tag in hp.edges["A"]["B"], "(A, B, tag) edges should've maintained."
            assert (
                tag1 in hp.edges["A"]["B"]
            ), "(A, B, tag1) edges should've maintained."
            assert tag not in hp.edges["B"]["A"], "(B, A, tag) edges should've dropped."
            assert tag in hp.edges["A"]["C"]
            assert tag in hp.edges["C"]["A"]
            assert tag in hp.edges["B"]["C"]
            assert tag in hp.edges["C"]["B"]

        elif reset == "tag_bidirectional":
            hp.reset_edges(axis_id_1="A", axis_id_2="B", tag=tag)
            assert tag not in hp.edges["A"]["B"], "(A, B, tag) edges should've dropped."
            assert tag not in hp.edges["B"]["A"], "(B, A, tag) edges should've dropped."
            assert (
                tag1 in hp.edges["A"]["B"]
            ), "(A, B, tag1) edges should've maintained."
            assert tag in hp.edges["A"]["C"]
            assert tag in hp.edges["C"]["A"]
            assert tag in hp.edges["B"]["C"]
            assert tag in hp.edges["C"]["B"]

        elif reset == "axis_pair":
            hp.reset_edges(axis_id_1="A", axis_id_2="B")
            assert "B" not in hp.edges["A"], "All (A, B) edges should've dropped."
            assert "A" not in hp.edges["B"], "All (B, A) edges should've dropped."
            assert tag in hp.edges["A"]["C"]
            assert tag in hp.edges["C"]["A"]
            assert tag in hp.edges["B"]["C"]
            assert tag in hp.edges["C"]["B"]

        elif reset == "axis_pair_one_direction":
            hp.reset_edges(axis_id_1="A", axis_id_2="B", a2_to_a1=False)
            assert "B" not in hp.edges["A"], "All (A, B) edges should've dropped."
            assert tag in hp.edges["B"]["A"]
            assert tag in hp.edges["A"]["C"]
            assert tag in hp.edges["C"]["A"]
            assert tag in hp.edges["B"]["C"]
            assert tag in hp.edges["C"]["B"]

        elif reset == "entire_from_axis":
            hp.reset_edges(axis_id_1="A", a2_to_a1=False)
            assert "A" not in hp.edges, "All (A, *) edges should've dropped."
            assert tag in hp.edges["B"]["A"]
            assert tag in hp.edges["C"]["A"]
            assert tag in hp.edges["B"]["C"]
            assert tag in hp.edges["C"]["B"]

        elif reset == "entire_to_axis":
            hp.reset_edges(axis_id_1="A", a1_to_a2=False)
            for a0 in hp.edges:
                assert "A" not in hp.edges[a0], "All (*, A) edges should've dropped."
            assert tag in hp.edges["A"]["C"]
            assert tag in hp.edges["A"]["B"]
            assert tag in hp.edges["B"]["C"]
            assert tag in hp.edges["C"]["B"]

        elif reset == "entire_axis":
            hp.reset_edges(axis_id_1="A")
            assert "A" not in hp.edges, "All (A, *) edges should've dropped."
            for a0 in hp.edges:
                assert "A" not in hp.edges[a0], "All (*, A) edges should've dropped."
            assert tag in hp.edges["B"]["C"]
            assert tag in hp.edges["C"]["B"]

        # should delete all edges
        elif reset == "all_edges":
            hp.reset_edges()
            assert (
                len(list(hp.edges.keys())) == 0
            ), "Should have no edge information at this point."

        elif reset == "invalid":
            # do just tag as example of invalid option
            try:
                hp.reset_edges(tag=tag)
                pytest.fail("Should've triggerd NotImplementedError")
            except NotImplementedError:
                assert True

        elif reset == "tag_not_found":
            try:
                hp.reset_edges(axis_id_1="A", axis_id_2="B", tag="non_existent_tag")
                pytest.fail("Should've triggerd ValueError from non existent tag")
            except ValueError:
                assert True

            # make sure it works both ways (bidirectional non existent will always fail on on the a1_to_a2 side
            try:
                hp.reset_edges(
                    axis_id_1="A", axis_id_2="B", tag="non_existent_tag", a1_to_a2=False
                )
                pytest.fail("Should've triggerd ValueError from non existent tag")
            except ValueError:
                assert True

        else:
            raise NotImplementedError

    @pytest.mark.parametrize("edge_method", ["curves", "kwargs"])
    def test_add_edge_curves_or_kwargs_trigger_keyerror(self, edge_method: str) -> None:
        """
        Test that ``HivePlot.add_edge_curves_between_axes()`` and ``HivePlot.add_edge_kwargs()`` trigger a ``KeyError``.

        Specifically when attempting to run between axes with unspecified edge IDs between which to connect.

        :param edge_method: string specifying which method to call.
        """
        hp, edges = three_axis_hiveplot_example()

        if edge_method == "curves":
            call = hp.add_edge_curves_between_axes
        elif edge_method == "kwargs":
            call = hp.add_edge_kwargs
        else:
            raise NotImplementedError

        # certainly if we haven't specified any edge IDs to connect, we will trigger the KeyError
        try:
            call(axis_id_1="A", axis_id_2="B", a2_to_a1=False, tag=0)
            pytest.fail("Should have triggered KeyError.")
        except KeyError:
            assert True

        # also, if we haven't specified a certain direction but ask for those curves,
        #  we should still trigger the KeyError
        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B", a2_to_a1=False)

        # a1 to a2 should not trigger an error, but a2 to a1 should since the IDs weren't created
        try:
            # only A -> B, which exists
            call(axis_id_1="A", axis_id_2="B", a2_to_a1=False, tag=tag)
            assert True
        except KeyError:
            pytest.fail(
                "These edge IDs were already specified, and therefore should not have triggered a KeyError."
            )

        try:
            # only B -> A, which does not exist
            call(axis_id_1="A", axis_id_2="B", a1_to_a2=False, tag=tag)
            pytest.fail(
                "These edge IDs were NOT yet specified, and therefore this should have triggered a KeyError."
            )
        except KeyError:
            assert True

    def test_add_edge_curves_between_axes_at_least_one_direction_of_edges(self) -> None:
        """
        ``HivePlot.add_edge_curves_between_axes()`` must specify at least one of ``a1_to_a2`` or ``a2_to_a1`` as True.
        """
        hp, edges = three_axis_hiveplot_example()

        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")

        try:
            hp.add_edge_curves_between_axes(
                axis_id_1="A", axis_id_2="B", a1_to_a2=False, a2_to_a1=False, tag=tag
            )
            pytest.fail("Un-allowed behavior. Should have triggered a ValueError.")
        except ValueError:
            assert True

    @pytest.mark.parametrize(
        "edge_directions", [[True, True], [True, False], [False, True]]
    )
    def test_add_edge_curves_between_axes_no_tag(self, edge_directions: list) -> None:
        """
        Make sure ``HivePlot.add_edge_curves_between_axes()`` tolerates no tag when there's one tag (and only 1).

        :param edge_directions: list specifying the parameter values `a1_to_a2` and `a2_to_a1` for
            ``add_edge_curves_between_axes()``.
        """
        a1_to_a2 = edge_directions[0]
        a2_to_a1 = edge_directions[1]

        hp, edges = three_axis_hiveplot_example()

        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")

        hp.add_edge_curves_between_axes(axis_id_1="A", axis_id_2="B")
        assert "curves" in hp.edges["A"]["B"][tag]

        tag2 = hp.add_edge_ids(
            edges=edges, axis_id_1="A", axis_id_2="B", tag="second_tag"
        )
        try:
            hp.add_edge_curves_between_axes(
                axis_id_1="A", axis_id_2="B", a1_to_a2=a1_to_a2, a2_to_a1=a2_to_a1
            )
            pytest.fail(
                "Should be ambiguous which tag to update and trigger a ValueError"
            )
        except ValueError:
            assert True

        assert "curves" not in hp.edges["A"]["B"][tag2]
        hp.add_edge_curves_between_axes(axis_id_1="A", axis_id_2="B", tag=tag2)
        assert "curves" in hp.edges["A"]["B"][tag2]

    def test_add_edge_kwargs_preserve_existing_kwargs(self) -> None:
        """
        Make sure ``HivePlot.add_edge_kwargs()`` doesn't drop previously set kwargs with multiple calls that add kwargs.
        """
        hp, edges = three_axis_hiveplot_example()
        tag = hp.connect_axes(edges=edges, axis_id_1="A", axis_id_2="B", c="blue")
        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", lw=2)
        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", ls="--")
        assert sorted(hp.edges["A"]["B"][tag]["edge_kwargs"]) == ["c", "ls", "lw"]

    def test_add_edge_ids_non_unique_tag(self) -> None:
        """
        Make sure ``HivePlot.add_edge_ids()`` yells when trying to add a non-unique tag of edges to an axis pair.
        """
        hp, edges = three_axis_hiveplot_example()
        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")
        hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B", tag="new_tag")
        try:
            hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B", tag=tag)
            pytest.fail(
                "Should yell for trying to add non-unique tag of edges to an axis pair"
            )
        except ValueError:
            assert True

    @pytest.mark.parametrize(
        "edge_directions", [[True, True], [True, False], [False, True]]
    )
    def test_add_edge_kwargs_no_tag(self, edge_directions: list) -> None:
        """
        Make sure ``HivePlot.add_edge_kwargs()`` tolerates no tag when there's one tag (and only 1).

        :param edge_directions: list specifying the parameter values `a1_to_a2` and `a2_to_a1` for
            ``add_edge_kwargs()``.
        """
        a1_to_a2 = edge_directions[0]
        a2_to_a1 = edge_directions[1]

        hp, edges = three_axis_hiveplot_example()

        tag = hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B")

        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", c="blue")
        assert "edge_kwargs" in hp.edges["A"]["B"][tag]

        tag2 = hp.add_edge_ids(
            edges=edges, axis_id_1="A", axis_id_2="B", tag="second_tag"
        )
        try:
            hp.add_edge_kwargs(
                axis_id_1="A",
                axis_id_2="B",
                a1_to_a2=a1_to_a2,
                a2_to_a1=a2_to_a1,
                c="black",
            )
            pytest.fail(
                "Should be ambiguous which tag to update and trigger a ValueError"
            )
        except ValueError:
            assert True

        assert "edge_kwargs" not in hp.edges["A"]["B"][tag2]
        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", tag=tag2)
        assert "edge_kwargs" in hp.edges["A"]["B"][tag2]

    def test_add_edge_kwargs_no_edges(self) -> None:
        """
        Make sure ``HivePlot.add_edge_kwargs()`` warns / runs as expected when there are no edges.
        """
        # generate synthetic node data from a pandas dataframe
        num_nodes = 15
        rng = np.random.default_rng(0)
        data = pd.DataFrame(
            np.c_[
                rng.uniform(low=0, high=10, size=num_nodes),
                rng.uniform(low=10, high=20, size=num_nodes),
                rng.uniform(low=20, high=30, size=num_nodes),
            ],
            columns=["low", "med", "high"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        # generate random edges
        num_edges = 0

        edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        hp = hive_plot_n_axes(
            node_list=nodes,
            edges=edges,
            axes_assignments=[
                np.arange(num_nodes)[: num_nodes // 3],
                np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                np.arange(num_nodes)[2 * num_nodes // 3 :],
            ],
            sorting_variables=["low", "med", "high"],
            repeat_axes=[True] * 3,
            axes_names=["Low", "Medium", "High"],
            orient_angle=-30,
            all_edge_kwargs={"alpha": 1},
        )

        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.add_edge_kwargs(axis_id_1="High", axis_id_2="Medium_repeat", c="C6")

        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.add_edge_kwargs(axis_id_1="High", axis_id_2="High_repeat", c="C6")

        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp.add_edge_kwargs(axis_id_1="High_repeat", axis_id_2="High", c="C6")

        # remove just the tagged edges to get specific warnings
        hp.reset_edges(axis_id_1="Medium", axis_id_2="Low_repeat", tag=0)
        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="Medium", axis_id_2="Low_repeat", c="C6")
        assert len(record) == 2, (
            "Should have yelled at both directions for having no tagged edges"
            + f"\n{[i.message.args for i in record]}"
        )

        # remove just some edge structure to get specific warnings
        hp.reset_edges(axis_id_1="Low")

        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="High_repeat", axis_id_2="Low", c="C6")
        assert len(record) == 2, (
            "Should have yelled at both directions for having no edges"
            + f"\n{[i.message.args for i in record]}"
        )

        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="Low", axis_id_2="High_repeat", c="C6")
        assert len(record) == 2, (
            "Should have yelled at both directions for having no edges"
            + f"\n{[i.message.args for i in record]}"
        )

        # remove all edge structure, same warnings should persist without errors running
        hp.reset_edges()

        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="High", axis_id_2="Medium_repeat", c="C6")
        assert len(record) == 2, (
            "Should have yelled at both directions for having no edges"
            + f"\n{[i.message.args for i in record]}"
        )

        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="High", axis_id_2="High_repeat", c="C6")
        assert len(record) == 1, (
            "Should have yelled once about repeat axes specifically"
            + f"\n{[i.message.args for i in record]}"
        )

        with pytest.warns() as record:
            hp.add_edge_kwargs(axis_id_1="High_repeat", axis_id_2="High", c="C6")
        assert len(record) == 1, (
            "Should have yelled once about repeat axes specifically"
            + f"\n{[i.message.args for i in record]}"
        )

    def test_copy(self) -> None:
        """
        Make sure ``HivePlot.copy()`` plays nice.
        """
        hp, edges = three_axis_hiveplot_example()

        hp_copy = hp.copy()

        hp.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B", a1_to_a2=False)

        assert len(list(hp_copy.edges.keys())) == 0
        assert len(list(hp.edges.keys())) == 1

        hp_copy.add_edge_ids(edges=edges, axis_id_1="A", axis_id_2="B", a2_to_a1=False)

        assert len(list(hp_copy.edges.keys())) == 1
        assert "A" in hp_copy.edges

        assert len(list(hp.edges.keys())) == 1
        assert "B" in hp.edges

    def test_to_json(self) -> None:
        """
        Make sure ``hiveplotlib.HivePlot.to_json()`` returns all the relevant information without error.
        """
        # generate synthetic node data from a pandas dataframe
        num_nodes = 15
        rng = np.random.default_rng(0)
        data = pd.DataFrame(
            np.c_[np.ones(num_nodes), np.ones(num_nodes), np.ones(num_nodes)],
            columns=["low", "med", "high"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        # generate random edges
        num_edges = 1

        edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        # force all nodes to end points of each axis
        hp = hive_plot_n_axes(
            node_list=nodes,
            edges=edges,
            axes_assignments=[
                np.arange(num_nodes)[: num_nodes // 3],
                np.arange(num_nodes)[num_nodes // 3 : 2 * num_nodes // 3],
                np.arange(num_nodes)[2 * num_nodes // 3 :],
            ],
            sorting_variables=["low", "med", "high"],
            repeat_axes=[True] * 3,
            axes_names=["Low", "Medium", "High"],
            orient_angle=-30,
            vmins=[0] * 3,
            vmaxes=[1] * 3,
            all_edge_kwargs={"alpha": 1, "c": "black"},
        )

        hp_json = hp.to_json()

        # check our outputs
        hp_dict = json.loads(hp_json)

        assert sorted(hp_dict.keys()) == ["axes", "edges"]

        for axis in hp_dict["axes"]:
            # check axes locations preserved
            assert list(hp.axes[axis].start) == hp_dict["axes"][axis]["start"]
            assert list(hp.axes[axis].end) == hp_dict["axes"][axis]["end"]

            # check node locations in Cartesian space preserved
            for val in ["x", "y"]:
                json_values = hp_dict["axes"][axis]["nodes"][val]
                # should all be same position
                assert np.unique(json_values).size == 1
                # should be same as what's in the hive plot
                assert json_values[0] == hp.axes[axis].node_placements[val][0]

        # check the one edge we drew
        arr_list = [
            np.array(arr).astype(float)
            for arr in hp_dict["edges"]["High"]["Medium_repeat"]["0"]["curves"]
        ]
        # add the NaN values back in to each curve to be comparable
        arr_list = [np.row_stack([arr, [np.nan, np.nan]]) for arr in arr_list]
        # concatenate back together
        arr = np.row_stack(arr_list)
        assert np.allclose(
            arr,
            hp.edges["High"]["Medium_repeat"][0]["curves"],
            rtol=0.0,
            atol=0.0,
            equal_nan=True,
        )
        assert arr.shape == (101, 2)
        # starts at "High" endpoint
        assert list(arr[0, :]) == list(hp.axes["High"].end)
        # ends at "Medium_repeat" endpoint (remember the NaN spacer so index -2)
        assert list(arr[-2, :]) == list(hp.axes["Medium_repeat"].end)

        # check the kwargs
        assert hp_dict["edges"]["High"]["Medium_repeat"]["0"]["edge_kwargs"] == {
            "alpha": 1,
            "c": "black",
        }

    def test_to_json_example_hp(self) -> None:
        """
        Make sure ``hiveplotlib.HivePlot.to_json()`` returns the same output as stored on disk.

        This output is used in documentation (linking to the file as a URL from Gitlab), so this test will check that we
        maintain that file accordingly.
        """
        hp = example_hive_plot(num_nodes=15, num_edges=30, seed=0)
        hp_json_as_dict = json.loads(hp.to_json())

        with open("./data/example_hive_plot.json", "r") as f:
            expected_hp_json_as_dict = json.load(f)

        message = (
            "Expected Hive Plot JSON different from actual hive plot JSON on disk. This could have happened for a few "
            "reasons.\n"
            "1. Something has changed with the ``hiveplotlib.dataseets.example_hive_plot()`` method.\n"
            "2. Something has changed with the ``hiveplotlib.HivePlot.to_json()`` method.\n"
            "3. Something has changed with lower-level hive plot calculations / data structures.\n"
            "4. A package dependency has changed the values somehow (e.g. sig figs, or 'code likes to break'...).\n\n"
            "If this change was *INTENTIONAL*, simply re-run the following python snippet from the root of the "
            "repository:\n\n"
            "```{python}\n"
            "from hiveplotlib.datasets import example_hive_plot\n"
            "hp = example_hive_plot(num_nodes=15, num_edges=30, seed=0)\n"
            "hp_json = hp.to_json()\n"
            "with open('./data/example_hive_plot.json', 'w') as f:\n"
            "    f.write(hp_json)\n"
            "```\n"
        )

        assert hp_json_as_dict == expected_hp_json_as_dict, message


class TestHivePlotNAxesKwargs:
    """
    Tests that various kwarg options in ``hive_plot_n_axes()`` are cooperating.
    """

    def setup_method(self) -> None:
        """
        Perform setup.
        """
        rng = np.random.default_rng(0)

        num_nodes = 51
        num_edges = 100

        # a and b columns move in sync with eventual node id (index), c is random
        data = pd.DataFrame(
            np.c_[
                np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
                np.sort(rng.uniform(low=0, high=10, size=num_nodes)),
                rng.uniform(low=0, high=10, size=num_nodes),
            ],
            columns=["a", "b", "c"],
        )

        # convert into dictionaries for later use
        node_data = data.to_dict(orient="records")

        # use the dataframe's index as unique id
        node_ids = data.index.to_numpy()

        self.nodes = [
            Node(unique_id=node_id, data=node_data[i])
            for i, node_id in enumerate(node_ids)
        ]

        self.edges = rng.choice(node_ids, size=num_edges * 2).reshape(-1, 2)

        # randomly split node ids into 3 groups
        axes_assignments = node_ids.copy()
        rng.shuffle(axes_assignments)
        self.axes_assignments = list(axes_assignments.reshape(3, -1))

        self.sorting_variables = ["a", "b", "c"]
        self.axes_names = ["A", "B", "C"]

    def test_all_edge_kwargs(self) -> None:
        """
        Make sure the ``all_edge_kwargs`` parameter behaves as expected.
        """
        hp = hive_plot_n_axes(
            node_list=self.nodes,
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
        )

        # shouldn't have any dotted line styles
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                if "ls" in hp.edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No lines should be dotted here"

        hp = hive_plot_n_axes(
            node_list=self.nodes,
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            all_edge_kwargs={"ls": "dotted"},
            ccw_edge_kwargs={},  # ccw_edge_kwargs defaults to dashed
        )

        # EVERY line should have dotted style
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                assert (
                    hp.edges[a0][a1][0]["edge_kwargs"]["ls"] == "dotted"
                ), "All lines should be dotted here"

    def test_cw_edge_kwargs(self) -> None:
        """
        Make sure the ``cw_edge_kwargs`` parameter behaves as expected.
        """
        hp = hive_plot_n_axes(
            node_list=self.nodes,
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
        )

        # shouldn't have any dotted line styles
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                if "ls" in hp.edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No lines should be dotted here"

        hp = hive_plot_n_axes(
            node_list=self.nodes,
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            cw_edge_kwargs={"ls": "dotted"},
            ccw_edge_kwargs={},  # ccw_edge_kwargs defaults to dashed
        )

        # EVERY cw line should have dotted style
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                if (a0, a1) in [("B", "A"), ("C", "B"), ("A", "C")]:
                    assert (
                        hp.edges[a0][a1][0]["edge_kwargs"]["ls"] == "dotted"
                    ), "All cw lines should be dotted here"
                elif "ls" in hp.edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No ccw lines should be dotted here"

    def test_ccw_edge_kwargs(self) -> None:
        """
        Make sure the ``ccw_edge_kwargs`` parameter behaves as expected.
        """
        hp = hive_plot_n_axes(
            node_list=self.nodes,
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
        )

        # shouldn't have any dotted line styles
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                if "ls" in hp.edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No lines should be dotted here"

        hp = hive_plot_n_axes(
            node_list=self.nodes,
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            ccw_edge_kwargs={"ls": "dotted"},
        )

        # EVERY ccw line should have dotted style
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                if (a0, a1) in [("A", "B"), ("B", "C"), ("C", "A")]:
                    assert (
                        hp.edges[a0][a1][0]["edge_kwargs"]["ls"] == "dotted"
                    ), "All ccw lines should be dotted here"
                elif "ls" in hp.edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No cw lines should be dotted here"

    def test_repeat_edge_kwargs(self) -> None:
        """
        Make sure the ``repeat_edge_kwargs`` parameter behaves as expected.
        """
        hp = hive_plot_n_axes(
            node_list=self.nodes,
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            repeat_axes=[True, True, True],
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
        )

        # shouldn't have any dotted line styles
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                if "ls" in hp.edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No lines should be dotted here"

        hp = hive_plot_n_axes(
            node_list=self.nodes,
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            repeat_axes=[True, True, True],
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            repeat_edge_kwargs={"ls": "dotted"},
        )

        # EVERY repeat axis line should have dotted style
        repeat_axes = ["A_repeat", "B_repeat", "C_repeat"]
        repeat_axis_pairs = list(zip(self.axes_names, repeat_axes)) + list(
            zip(repeat_axes, self.axes_names)
        )
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                if (a0, a1) in repeat_axis_pairs:
                    assert (
                        hp.edges[a0][a1][0]["edge_kwargs"]["ls"] == "dotted"
                    ), "All repeat lines should be dotted here"
                elif "ls" in hp.edges[a0][a1][0]["edge_kwargs"]:
                    assert (
                        hp.edges[a0][a1][0]["edge_kwargs"]["ls"] != "dotted"
                    ), "No non-repeat lines should be dotted here"

        # make sure we trigger warnings on redundant kwargs, check kwargs

        with pytest.warns() as record:
            hp = hive_plot_n_axes(
                node_list=self.nodes,
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                all_edge_kwargs={"c": "red"},
                repeat_edge_kwargs={"c": "blue"},
            )

        assert (
            len(record) > 0
        ), "Expected warnings per pair of repeat axes each direction to connect about conflicting kwargs"
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                for i in range(2):
                    if (a0, a1) in repeat_axis_pairs:
                        assert (
                            hp.edges[a0][a1][i]["edge_kwargs"]["c"] == "blue"
                        ), "Wrong kwarg"
                    else:
                        assert (
                            hp.edges[a0][a1][i]["edge_kwargs"]["c"] == "red"
                        ), "Wrong kwarg"

        # check that unique all_edge_kwargs propagate as expected
        with warnings.catch_warnings():
            warnings.simplefilter("error")
            hp = hive_plot_n_axes(
                node_list=self.nodes,
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                all_edge_kwargs={"lw": 2},
                repeat_edge_kwargs={"c": "blue"},
            )
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                for i in range(2):
                    assert hp.edges[a0][a1][i]["edge_kwargs"]["lw"] == 2, "Wrong kwarg"

    def test_edge_list_kwargs(self) -> None:
        """
        Make sure ``edge_list_kwargs`` parameter behaves as expected.
        """
        hp = hive_plot_n_axes(
            node_list=self.nodes,
            edges=self.edges,
            axes_assignments=self.axes_assignments,
            repeat_axes=[True, True, True],
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            ccw_edge_kwargs={},
            edge_list_kwargs=None,
        )
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                assert (
                    len(list(hp.edges[a0][a1][0]["edge_kwargs"].keys())) == 0
                ), f"Found edge kwargs but expected None {hp.edges[a0][a1][0]['edge_kwargs']}"

        hp = hive_plot_n_axes(
            node_list=self.nodes,
            edges=[self.edges] * 2,
            axes_assignments=self.axes_assignments,
            repeat_axes=[True, True, True],
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            ccw_edge_kwargs={},
            edge_list_kwargs=None,
        )
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                assert (
                    len(list(hp.edges[a0][a1][0]["edge_kwargs"].keys())) == 0
                ), "Shouldn't have any edge kwargs"

        hp = hive_plot_n_axes(
            node_list=self.nodes,
            edges=[self.edges] * 2,
            axes_assignments=self.axes_assignments,
            repeat_axes=[True, True, True],
            sorting_variables=self.sorting_variables,
            axes_names=self.axes_names,
            ccw_edge_kwargs={},
            edge_list_kwargs=[{"c": "blue"}, None],
        )
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                assert hp.edges[a0][a1][0]["edge_kwargs"]["c"] == "blue", "Wrong kwarg"
                assert (
                    len(list(hp.edges[a0][a1][1]["edge_kwargs"].keys())) == 0
                ), "Shouldn't have any edge kwargs"

        try:
            hive_plot_n_axes(
                node_list=self.nodes,
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                edge_list_kwargs=[{"c": "blue"}] * 3,
            )
            raise ValueError("Shouldn't allow wrong number of `edge_list_kwargs`")
        except AssertionError:
            assert True

        # repeat_edge_kwargs clashes with edge_list_kwargs
        with pytest.warns() as record:
            hp = hive_plot_n_axes(
                node_list=self.nodes,
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                repeat_edge_kwargs={"c": "red"},
                edge_list_kwargs=[{"c": "blue"}] * 2,
            )

        assert (
            len(record) > 0
        ), "Expected warnings per pair of axes each direction to connect about conflicting kwargs"
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                for i in range(2):
                    assert (
                        hp.edges[a0][a1][i]["edge_kwargs"]["c"] == "blue"
                    ), "Wrong kwarg"

        # all_edge_kwargs clashes with edge_list_kwargs
        with pytest.warns() as record:
            hp = hive_plot_n_axes(
                node_list=self.nodes,
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                all_edge_kwargs={"c": "red"},
                edge_list_kwargs=[{"c": "blue"}] * 2,
            )

        assert (
            len(record) > 0
        ), "Expected warnings per pair of axes each direction to connect about conflicting kwargs"
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                for i in range(2):
                    assert (
                        hp.edges[a0][a1][i]["edge_kwargs"]["c"] == "blue"
                    ), "Wrong kwarg"

        # cw_edge_kwargs clashes with edge_list_kwargs
        with pytest.warns() as record:
            hp = hive_plot_n_axes(
                node_list=self.nodes,
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={},
                cw_edge_kwargs={"c": "red"},
                edge_list_kwargs=[{"c": "blue"}] * 2,
            )

        assert (
            len(record) > 0
        ), "Expected warnings per pair of axes each direction to connect about conflicting kwargs"
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                for i in range(2):
                    assert (
                        hp.edges[a0][a1][i]["edge_kwargs"]["c"] == "blue"
                    ), "Wrong kwarg"

        # ccw_edge_kwargs clashes with edge_list_kwargs
        with pytest.warns() as record:
            hp = hive_plot_n_axes(
                node_list=self.nodes,
                edges=[self.edges] * 2,
                axes_assignments=self.axes_assignments,
                repeat_axes=[True, True, True],
                sorting_variables=self.sorting_variables,
                axes_names=self.axes_names,
                ccw_edge_kwargs={"c": "red"},
                edge_list_kwargs=[{"c": "blue"}] * 2,
            )

        assert (
            len(record) > 0
        ), "Expected warnings per pair of axes each direction to connect about conflicting kwargs"
        for a0 in hp.edges:
            for a1 in hp.edges[a0]:
                for i in range(2):
                    assert (
                        hp.edges[a0][a1][i]["edge_kwargs"]["c"] == "blue"
                    ), "Wrong kwarg"


class TestMultipleEdgeSets:
    """
    Tests focused on checking for appropriate behavior when adding multiple sets of edges between one pair of axes.
    """

    def test_add_edge_ids_2_sets(self) -> None:
        """
        Check that ``HivePlot.add_edge_ids()`` plays nice when adding 2 sets of edges.
        """
        hp, edges = three_axis_hiveplot_example()

        num_edges = edges.shape[0]
        tag_0 = hp.add_edge_ids(
            edges=edges[:num_edges, :], axis_id_1="A", axis_id_2="B", tag=5
        )
        assert tag_0 == 5
        assert list(hp.edges["A"]["B"].keys()) == [tag_0]
        assert list(hp.edges["B"]["A"].keys()) == [tag_0]

        tag_1 = hp.add_edge_ids(
            edges=edges[num_edges:, :],
            axis_id_1="A",
            axis_id_2="B",
            tag=10,
            a2_to_a1=False,
        )
        assert tag_1 == 10
        assert sorted(hp.edges["A"]["B"].keys()) == [tag_0, tag_1]
        assert list(hp.edges["B"]["A"].keys()) == [
            tag_0
        ], f"Should not have added B -> A with tag {tag_1}"

    @pytest.mark.parametrize("a2_to_a1", [True, False])
    def test_add_edge_curves_between_axes_2_sets(self, a2_to_a1: bool) -> None:
        """
        Check tht ``HivePlot.add_edge_curves_between_axes()`` plays nice when adding 2 sets of edges.

        :param a2_to_a1: whether to construct the axis 2 -> axis 1 curves (True) or not (False).
        """
        hp, edges = three_axis_hiveplot_example()

        num_edges = edges.shape[0]
        # build out IDs in both directions, but check curves as we add
        tag_0 = hp.add_edge_ids(
            edges=edges[:num_edges, :], axis_id_1="A", axis_id_2="B", tag=5
        )
        tag_1 = hp.add_edge_ids(
            edges=edges[num_edges:, :], axis_id_1="A", axis_id_2="B", tag=10
        )

        hp.add_edge_curves_between_axes(
            axis_id_1="A", axis_id_2="B", tag=tag_0, a2_to_a1=a2_to_a1
        )
        assert "curves" in hp.edges["A"]["B"][tag_0]
        a_b_tag_0_curves = hp.edges["A"]["B"][tag_0]["curves"].copy()
        if a2_to_a1:
            assert "curves" in hp.edges["B"]["A"][tag_0]
        else:
            assert "curves" not in hp.edges["B"]["A"][tag_0]

        assert (
            "curves" not in hp.edges["A"]["B"][tag_1]
        ), f"Tag {tag_1} curves should not exist yet"
        assert (
            "curves" not in hp.edges["B"]["A"][tag_1]
        ), f"Tag {tag_1} curves should not exist yet"

        hp.add_edge_curves_between_axes(
            axis_id_1="A", axis_id_2="B", tag=tag_1, a2_to_a1=a2_to_a1
        )

        # check equal arrays but tolerating the nan spacers
        assert np.allclose(
            a_b_tag_0_curves,
            hp.edges["A"]["B"][tag_0]["curves"],
            rtol=0.0,
            atol=0.0,
            equal_nan=True,
        ), f"Tag {tag_0} curves should not be altered by doing anything to Tag {tag_1} curves"

        assert (
            "curves" in hp.edges["A"]["B"][tag_1]
        ), f"Tag {tag_1} curves should now exist"
        if a2_to_a1:
            assert (
                "curves" in hp.edges["B"]["A"][tag_1]
            ), f"Tag {tag_1} curves should now exist"
        else:
            assert (
                "curves" not in hp.edges["B"]["A"][tag_1]
            ), f"Tag {tag_1} curves should not exist in this case"

    def test_construct_curves_2_sets(self) -> None:
        """
        Check that ``HivePlot.construct_curves()`` plays nice when adding 2 sets of edges.
        """
        hp, edges = three_axis_hiveplot_example()

        num_edges = edges.shape[0]
        # build out IDs in both directions, but check curves as we add
        tag_0 = hp.add_edge_ids(
            edges=edges[:num_edges, :], axis_id_1="A", axis_id_2="B", tag=5
        )
        tag_1 = hp.add_edge_ids(
            edges=edges[num_edges:, :], axis_id_1="A", axis_id_2="B", tag=10
        )

        assert "curves" not in hp.edges["A"]["B"][tag_0]
        assert "curves" not in hp.edges["A"]["B"][tag_1]
        assert "curves" not in hp.edges["B"]["A"][tag_0]
        assert "curves" not in hp.edges["B"]["A"][tag_1]

        hp.construct_curves()

        assert "curves" in hp.edges["A"]["B"][tag_0]
        assert "curves" in hp.edges["A"]["B"][tag_1]
        assert "curves" in hp.edges["B"]["A"][tag_0]
        assert "curves" in hp.edges["B"]["A"][tag_1]

    def test_add_edge_kwargs_2_sets(self) -> None:
        """
        Check that ``HivePlot.add_edge_kwargs()`` plays nice when there are 2 sets of edges.
        """
        hp, edges = three_axis_hiveplot_example()

        num_edges = edges.shape[0]
        # build out IDs in both directions, but check curves as we add
        tag_0 = hp.add_edge_ids(
            edges=edges[:num_edges, :], axis_id_1="A", axis_id_2="B", tag=5
        )
        tag_1 = hp.add_edge_ids(
            edges=edges[num_edges:, :], axis_id_1="A", axis_id_2="B", tag=10
        )

        assert "edge_kwargs" not in hp.edges["A"]["B"][tag_0]
        assert "edge_kwargs" not in hp.edges["B"]["A"][tag_0]

        assert "edge_kwargs" not in hp.edges["A"]["B"][tag_1]
        assert "edge_kwargs" not in hp.edges["B"]["A"][tag_1]

        try:
            hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", tag=None)
            pytest.fail(
                "Should've triggered an error for not specifying a non-None tag"
            )
        except ValueError:
            assert True

        # just tag_0 changes first
        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", tag=tag_0, c="cornflowerblue")

        assert "edge_kwargs" in hp.edges["A"]["B"][tag_0]
        assert "edge_kwargs" in hp.edges["B"]["A"][tag_0]

        assert list(hp.edges["A"]["B"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert hp.edges["A"]["B"][tag_0]["edge_kwargs"]["c"] == "cornflowerblue"

        assert list(hp.edges["B"]["A"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert hp.edges["B"]["A"][tag_0]["edge_kwargs"]["c"] == "cornflowerblue"

        assert "edge_kwargs" not in hp.edges["A"]["B"][tag_1]
        assert "edge_kwargs" not in hp.edges["B"]["A"][tag_1]

        # one direction of tag_1 changes
        hp.add_edge_kwargs(
            axis_id_1="A", axis_id_2="B", a2_to_a1=False, tag=tag_1, c="seagreen"
        )

        assert "edge_kwargs" in hp.edges["A"]["B"][tag_1]
        assert "edge_kwargs" not in hp.edges["B"]["A"][tag_1]

        assert list(hp.edges["A"]["B"][tag_1]["edge_kwargs"].keys()) == ["c"]
        assert hp.edges["A"]["B"][tag_1]["edge_kwargs"]["c"] == "seagreen"

        # bidirectional tag_1 changes, switch the existing color too
        hp.add_edge_kwargs(axis_id_1="A", axis_id_2="B", tag=tag_1, c="maroon", lw=2)

        assert "edge_kwargs" in hp.edges["A"]["B"][tag_1]
        assert "edge_kwargs" in hp.edges["B"]["A"][tag_1]

        assert sorted(hp.edges["A"]["B"][tag_1]["edge_kwargs"].keys()) == [
            "c",
            "lw",
        ]
        assert hp.edges["A"]["B"][tag_1]["edge_kwargs"]["c"] == "maroon"

        assert list(hp.edges["B"]["A"][tag_1]["edge_kwargs"].keys()) == ["c", "lw"]
        assert hp.edges["B"]["A"][tag_1]["edge_kwargs"]["c"] == "maroon"

        # tag_0 should be unaffected by all of this
        assert list(hp.edges["A"]["B"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert hp.edges["A"]["B"][tag_0]["edge_kwargs"]["c"] == "cornflowerblue"

        assert list(hp.edges["B"]["A"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert hp.edges["B"]["A"][tag_0]["edge_kwargs"]["c"] == "cornflowerblue"

    def test_connect_axes_2_sets(self) -> None:
        """
        Check that ``HivePlot.connect_axes()`` plays nice when there are 2 sets of edges.
        """
        hp, edges = three_axis_hiveplot_example()

        num_edges = edges.shape[0]

        tag_0 = hp.connect_axes(
            edges=edges[:num_edges, :], axis_id_1="A", axis_id_2="B", c="khaki"
        )
        assert tag_0 == 0
        tag_1 = hp.connect_axes(
            edges=edges[num_edges:, :],
            axis_id_1="A",
            axis_id_2="B",
            tag=10,
            c="darksalmon",
            lw=5,
        )

        assert list(hp.edges["A"]["B"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert hp.edges["A"]["B"][tag_0]["edge_kwargs"]["c"] == "khaki"

        assert list(hp.edges["B"]["A"][tag_0]["edge_kwargs"].keys()) == ["c"]
        assert hp.edges["B"]["A"][tag_0]["edge_kwargs"]["c"] == "khaki"

        assert sorted(hp.edges["A"]["B"][tag_1]["edge_kwargs"].keys()) == [
            "c",
            "lw",
        ]
        assert hp.edges["A"]["B"][tag_1]["edge_kwargs"]["c"] == "darksalmon"

        assert list(hp.edges["B"]["A"][tag_1]["edge_kwargs"].keys()) == ["c", "lw"]
        assert hp.edges["B"]["A"][tag_1]["edge_kwargs"]["c"] == "darksalmon"


def test_connect_axes_change_control_pt_rho() -> None:
    """
    Check that ``HivePlot.connect_axes()`` changes edges as expected when we alter the ``control_rho_scale`` parameter.
    """
    # set up example with a single edge
    num_axes = 2
    nodes, node_splits, edges = example_nodes_and_edges(
        num_axes=num_axes, num_edges=1, seed=1
    )

    hp = HivePlot()
    hp.add_nodes(nodes)

    for i in range(num_axes):
        temp_axis = Axis(
            axis_id=i,
            start=1,
            end=5,
            angle=30 + 120 * i,
            long_name=f"Axis {i}",
        )
        hp.add_axes(temp_axis)
        hp.place_nodes_on_axis(
            axis_id=i,
            unique_ids=node_splits[i],
            sorting_feature_to_use="low",
        )

    # make default hp edge first
    default_hp = hp.copy()
    default_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
    )

    # build lower rho edge
    low_rho_hp = hp.copy()
    low_rho_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
        control_rho_scale=0.5,
    )

    # build higher rho edge
    high_rho_hp = hp.copy()
    high_rho_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
        control_rho_scale=1.5,
    )

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    default_edge_cartesian = default_hp.edges[1][0][0]["curves"][1:-2, :]
    default_rhos = cartesian2polar(
        default_edge_cartesian[:, 0], default_edge_cartesian[:, 1]
    )[0]

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    low_rho_edge_cartesian = low_rho_hp.edges[1][0][0]["curves"][1:-2, :]
    low_rho_rhos = cartesian2polar(
        low_rho_edge_cartesian[:, 0], low_rho_edge_cartesian[:, 1]
    )[0]

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    high_rho_edge_cartesian = high_rho_hp.edges[1][0][0]["curves"][1:-2, :]
    high_rho_rhos = cartesian2polar(
        high_rho_edge_cartesian[:, 0], high_rho_edge_cartesian[:, 1]
    )[0]

    assert (low_rho_rhos < default_rhos).all()
    assert (default_rhos < high_rho_rhos).all()


def test_connect_axes_change_control_pt_angle() -> None:
    """
    Check that ``HivePlot.connect_axes()`` changes edges as expected when we alter the ``control_angle_shift`` param.
    """
    # set up example with a single edge
    num_axes = 2
    nodes, node_splits, edges = example_nodes_and_edges(
        num_axes=num_axes, num_edges=1, seed=1
    )

    hp = HivePlot()
    hp.add_nodes(nodes)

    for i in range(num_axes):
        temp_axis = Axis(
            axis_id=i,
            start=1,
            end=5,
            angle=30 + 120 * i,
            long_name=f"Axis {i}",
        )
        hp.add_axes(temp_axis)
        hp.place_nodes_on_axis(
            axis_id=i,
            unique_ids=node_splits[i],
            sorting_feature_to_use="low",
        )

    # make default hp edge first
    default_hp = hp.copy()
    default_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
    )

    # build lower angle edge
    low_angle_hp = hp.copy()
    low_angle_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
        control_angle_shift=-20,
    )

    # build higher angle edge
    high_angle_hp = hp.copy()
    high_angle_hp.connect_axes(
        edges=edges,
        axis_id_1=0,
        axis_id_2=1,
        control_angle_shift=20,
    )

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    default_edge_cartesian = default_hp.edges[1][0][0]["curves"][1:-2, :]
    default_angles = cartesian2polar(
        default_edge_cartesian[:, 0], default_edge_cartesian[:, 1]
    )[1]

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    low_angle_edge_cartesian = low_angle_hp.edges[1][0][0]["curves"][1:-2, :]
    low_angle_angles = cartesian2polar(
        low_angle_edge_cartesian[:, 0], low_angle_edge_cartesian[:, 1]
    )[1]

    # grab edge, but skip the first and last points (which will be equal because node points)
    #  plus, skip the nan, nan record at the end.
    high_angle_edge_cartesian = high_angle_hp.edges[1][0][0]["curves"][1:-2, :]
    high_angle_angles = cartesian2polar(
        high_angle_edge_cartesian[:, 0], high_angle_edge_cartesian[:, 1]
    )[1]

    assert (low_angle_angles < default_angles).all()
    assert (default_angles < high_angle_angles).all()
