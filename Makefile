.PHONY: activate black flake8 format docs test test-nb view-docs view-tests

.ONESHELL:
SHELL := /bin/bash

TEST_ARGS?=""

format:
	source activate hiveplot_env
	ruff format
	ruff check --fix

cleandocs:
	rm -f docs/source/*.ipynb

docs: cleandocs
	source activate hiveplot_env
	bash build_sphinx_docs.sh

docs-strict: cleandocs
	source activate hiveplot_env
	bash build_sphinx_docs -W

install:
	bash install.sh

test:
	source activate hiveplot_env
	pytest -c tests/pytest_hiveplotlib.ini $(TEST_ARGS)

test-all: test view-tests test-nb view-tests-nb

test-nb:
	source activate hiveplot_env
	pytest -c tests/pytest_examples.ini

uninstall:
	bash uninstall.sh

view-docs:
	google-chrome public/index.html

view-tests:
	google-chrome data/pytest/report_hiveplotlib.html

view-tests-nb:
	google-chrome data/pytest/report_examples.html
