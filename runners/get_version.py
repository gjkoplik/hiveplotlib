# get_version.py

"""
Reads off the current version from ``pyproject.toml`` so that version can be ``pip`` installed in a CI/CD job.
"""

import pathlib

import toml

pyproject_path = pathlib.Path("pyproject.toml")

print(toml.load(pyproject_path.open())["project"]["version"])
