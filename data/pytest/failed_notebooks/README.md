`pytest` failures testing that example Jupyter notebooks run to completion (e.g. `./tests/examples_test.py`) will
trigger an html copy of the failed notebook being saved to this directory.