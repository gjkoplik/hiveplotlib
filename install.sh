#!/bin/bash
# kill any old, existing env and notebook kernel
# use mamba for speed, fall back to conda if necessary
bash uninstall.sh || echo "Failed to Uninstall"
mamba env create -f hiveplot_env.yml || conda env create -f hiveplot_env.yml
# only do follow-up installation if environment can be activated (e.g. the previous line actually succeeded)
source activate hiveplot_env && \
  pip install -e .[dev,testing,docs,ruff] && \
  python -m ipykernel install --user --name hiveplot_env --display-name "Python (hiveplot_env)"
