# Contributing

## Developers

For the moment, we will restrict developers to employees of Geometric Data Analytics. If it becomes appropriate
given interest in contributing or growth of the project, we will (happily) revise this policy later. If you have
recommendations for changes / extensions or discover bugs, we would still appreciate your thoughts.

## Feature Requests / Bug Reports

Use the [Issue Tracker](https://gitlab.com/geomdata/hiveplotlib/-/issues) for feature requests or reporting bugs.

## Testing

Tests for the `hiveplotlib` package should be run in the `hiveplot_env` `conda` environment and associated `jupyter` kernel,
which can both be created by running:

```{bash}
cd <path/to/repository>
bash install.sh
```

Note, `mamba` or `conda` must be installed beforehand.

All tests can be run via:

```{bash}
cd <path/to/repository>
source activate hiveplot_env
bash run_tests.sh
```

Any code added to the `hiveplotlib` package should be tested, as evidenced by the coverage report
visible in the upper right of a clicked on "hiveplotlib" job in each pipeline, which should state "Coverage: 100%".
Coverage checks can also be looked at locally after running `bash run_tests.sh` by opening:

`<path/to/repository>/data/pytest/hiveplotlib_coverage.html/index.html`

## Code Hygiene Checks

[Ruff](https://docs.astral.sh/ruff/) is used for linting and formatting code.

### Linting

Linting refers to a static analysis of the codebase to check for efficient and compliant python code. The default checks
are documented in the [pyproject.toml](pyproject.toml) files. Ruff can be run from the command line via `ruff check` or
by installing ruff in an IDE. Certain errors in ruff can be automatically fixed by running `ruff check --fix`.

Any errors that pop up (e.g. E401) can be expanded on by executing `ruff rule <error>`, as in `ruff rule E401`.

If added code is compliant, then running `ruff check` should not print out any warnings.

This is tested in each CI/CD pipeline in the `lint_check` job.

### Formatting

Formatting refers to adjusting the physical appearance of the code (adding new lines, commas, etc.), without modifying
any of the intent or logic. Ruff can automatically format a specific file or an entire repository by executing
`ruff format <path>/<to>/<file>` or `ruff format`.

To pass pipelines, this must be run before pushing code.

This is tested in each CI/CD pipeline in the `format_check` job.

## Notebooks

Any `jupyter` notebooks  added to `examples` are automatically tested in the "examples" job in each CI/CD pipeline to confirm
that they run start to finish without error.

### Adding Notebooks to Documentation

These notebooks can also easily be added to the Gitlab pages
[documentation](https://geomdata.gitlab.io/hiveplotlib/) by including the name of your notebook (without the `.ipynb`) in
`docs/source/index.rst` (don't worry about generating your notebook as an `.rst` or `html` file, this is automatically handled by the CI).

Note, the new notebooks will not be visible in the public documentation until the code is merged back to `master` and the Gitlab pages documentation
is rebuilt.
